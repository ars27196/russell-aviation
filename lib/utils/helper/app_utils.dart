import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:intl/intl.dart';
import 'package:russellavaition/utils/elements/progress_hud.dart';
import 'package:universal_platform/universal_platform.dart';
import 'package:url_launcher/url_launcher.dart';

class AppUtils {
  static bool isIOS() {
//    return Platform.isIOS;
    return UniversalPlatform.isIOS;
  }

  static bool disableEmailForTime = false;

  static void openEmail({String userEmail, String subject, String body}) {
    if (AppUtils.isIOS()) {
      AppUtils.openUrl("mailto:$userEmail");
    } else {
      if (disableEmailForTime == false) {
        disableEmailForTime = true;
        Future.delayed(Duration(seconds: 2), () {
          disableEmailForTime = false;
        });
      } else {
        return;
      }
      final Email email = Email(
        body: body,
        subject: subject,
        recipients: [userEmail],
      );

      FlutterEmailSender.send(email);
    }
  }

  static void openUrl(String url) {
    launch(url ?? "");
  }

  static ProgressHUD progressHUD(
      {bool startLoad = false,
      bool forInternalViews = false,
      @required BuildContext context}) {
    ProgressHUD _progressHUD = new ProgressHUD(
      backgroundColor: Theme.of(context).primaryColor,
      color: Colors.white,
      containerColor: Colors.pinkAccent,
      borderRadius: 5.0,
      loading: startLoad,
      forInternalViews: forInternalViews,
    );
    return _progressHUD;
  }

  static bool showingLoader = false;
  static int loaderCount = 0;

  static void showLoader({@required BuildContext context}) {
    loaderCount++;
    debugPrint("showLoader $loaderCount");
    if (context == null || showingLoader) return;
    showingLoader = true;
    ProgressHUD _progressHUD = progressHUD(context: context, startLoad: true);
    showDialog(
        context: context,
        barrierDismissible: false,
        useRootNavigator: true,
        builder: (context) {
          return Center(
            child: Container(
              height: 100,
              width: 100,
              child: Stack(
                children: <Widget>[_progressHUD],
              ),
            ),
          );
        });
  }

  /// Dont call it if loader not showing
  static void dismissLoader({@required BuildContext context}) {
    loaderCount--;
    debugPrint("dismissLoader $loaderCount");
    if (loaderCount == 0 && context != null) {
      showingLoader = false;
      Navigator.of(context, rootNavigator: true).pop();
    }
  }

  // call to generate booking id
  static String generateBookingId() {
    return DateFormat("yyyyMMddHHmm").format(DateTime.now());
  }

  static bool adminEmailId(String email) => email == "ars27196@gmail.com";
}
