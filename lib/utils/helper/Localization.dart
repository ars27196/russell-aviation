import 'dart:ui';

import 'package:flutter/material.dart';

class Localization {
  static String stLocalized(String key, String language) {
    return localizedValues[language][key];
  }

  static final Map<String, Map<String, String>> localizedValues = {
  'en': {
  "validEmail": "Should be a valid Email address",
  "Email": "email",
  "password": "Password",
  "login": "Sign In",
  "logup": "Sign Up",
  "emailOrPassError": "Email or Password is incorrect",
  "uploadError": "Unable to upload. Please try again.",
  "moreThen3": "Should be more then 3 characters",
  "moreThen6": "Should be more then 6 characters",
  "noAccount": "I don't have an Account",
  "haveAccount": "I have an Account",
  "forgotPassword": "Forgot Password?",
  "skip": 'Skip',
  "letsStartLogin": "Lets Start with Login",
  "letsStartLogup": "Lets Start with Register",
  "completeLogup": "Complete Registration",
  "firstName": "First Name",
  "lastName": "Last Name",
  "johnDoe": "John Doe",
  "phone": "Phone Number",
  "register": "Register",
  "emailToResetPassword": "Enter Email to Reset Password",
  "resetPassword": "Press to Reset Password",
  "rememberPassword": "Remember Password? Return to Sign In",
  "validNumber": "Enter a Valid Number",
  "address": "Address",
  //side navigation drawer + bottom navigation bar
  "seatBooking": "Book Seat",
  "aboutUs": "About Us",
  "contactUs": "Contact Us",
  "bookingHistory": "View Previous Bookings",
  "preferences": "Preferences",
  "help": "Help",
  "setting": "Settings",
  "lightMode": "Light Mode",
  "darkMode": "Dark Mode",
  "logOut": "Log Out",
  "version": "Version 0.0.1",
  //about us
  "aboutUsMessage":
  "Flying private has always been reserved for the super-wealthy. Not anymore. Russell Aviation’s goal is to offer better services at even better prices. Without sacrificing any of the luxuries we have built our business tailored for everyone, one that never sacrifices luxury, while consistently giving our clients the best prices in the private charter business",

  //contact Us
  "name": "Name",
  "email": "Email",
  "message": "Message",
  "helpMessage": "What can we help you with?",
  "sendMessage": "Send Message",
  "subject": "Subject",

  //booking Seat
  "departureFrom": "Departure From",
  "arrivalAt": "Arrival At",
  "airport1": "Key West International\nAirport",
  "airport2": "Punta Gorda\nAirport",
  "city": "Florida",
  "oneWay": "One Way",
  "twoWay": "Two Way",
  "select": "Select",
  "departureDate": "Departure Date",
  "arrivalDate": "Arrival Date",
  "proceed": "Proceed",
  "selectDateError": "Select Date First.",
  "bookingError": "Error while Booking. Please Try Again",
  "fetchBookingError":
  "Error while fetching Booking History. Please Try Again",

  //date picker
  "done": "Done",
  "cancel": "Cancel",

  //trems conditions
  "term_condition":
  "Russell Aviation’s cancellation policy is designed to provide our clients with the most flexibility when arranging their trip. Because private aircraft charter is an on-demand business, we extend a 2 week cancellation policy on most regular charter flights. If the flight is cancelled within the contracted time frame, typically prior to the 2 week deadline from the booked fligh, Russell Aviation will extend a 100% refund for the amount collected.* There are specific instances in which Russell Aviation will book an aircraft at a special rate for our clients, or there may be specific details that do not allow us to extend the standard 2 week cancellation policy. In all cases, Russell Aviation will provide all of the cancellation and refund details specific to our client’s flights on the provided Russell Aviation charter agreement/contract. If you have any questions about Russell Aviation’s cancellation or refund policy, please feel free to contact our friendly staff at (941) 800-1177 *Restrictions and exceptions may apply. This policy is the standard cancellation policy and may vary depending on the specific details of the flight and price secured by Russell Aviation LLC. Specific cancellation policy will be described on the Russell Aviation agreement/contract at the time of booking.",
  "agree": "Agree",

  //confirm ticket
  "confirmBooking": "Confirm Seat Booking",

  // confirm Booking
  "proceedToPayment": "Proceed to Payement",
  "departureDateTime": "Departure Date/Time:",
  "arrivalDateTime": "Return Date/Time:",
  "advanceAmount": "Advance Amount",
  "totalAmount": "TotalAmount",

  // booking history
  "bookingHistoryTitle": "Booking History",
  "passengers": "Passenger",
  "noHistoryFound": "No Booking History Found",

    // booking details
  "bookingSeatDetails": "Seat Booking Details",
  "status": "Status",
  "bookerName" :"User Name",
  "bookerEmail" : "User Email",

    //payment success screen
    "success": "Success",
    "failure": "Failure",
    "successMessage": "Your Payment was successful. Please contact us before your flight to pay remaining amount and confirming your Flight.",
    "failureMessage": "Your Payment was unsuccessful. Please try again or contact us to resolve any issue.",
    "thanks":"Thanks",
    "moveToHomeScreen": "Move To Home Screen",
    "continue": "Continue",


},};

static TextDirection textDirectionLtr
() {
return TextDirection.rtl;
}

static TextDirection textDirectionRtl
() {
return TextDirection.rtl;
}

static TextAlign textAlignLeft
() {
return isEnglish() ? TextAlign.left : TextAlign.right;
}

static TextAlign textAlignRight
() {
return !isEnglish() ? TextAlign.left : TextAlign.right;
}

static Alignment alignmentTopLeft
() {
return !isEnglish() ? Alignment.topRight : Alignment.topLeft;
}

static Alignment alignmentTopRight
() {
return isEnglish() ? Alignment.topRight : Alignment.topLeft;
}

static FractionalOffset fractionalOffsetTopRight
() {
return isEnglish() ? FractionalOffset.topRight : FractionalOffset.topLeft;
}

static String defaultAlertTitle
() {
return stLocalized("alert_title", "en");
}

static String defaultAlertButtonTitle
() {
return stLocalized("default_alert_btn_title", "en");
}

static bool isEnglish
() {
return true;
}

static String isEnglishCode
() {
return "en";
}

static String isArabicCode
() {
return "ar";
}
}
