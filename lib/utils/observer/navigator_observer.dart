import 'package:flutter/cupertino.dart';

class RNavigatorObserver extends NavigatorObserver {

  int stackCount = 0;
  List<Route> routes = new List();
  @override
  void didPush(Route<dynamic> route, Route<dynamic> previousRoute) {
    super.didPush(route, previousRoute);
    stackCount++;
    debugPrint("didPush");
    routes.add(route);
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic> previousRoute) {

      super.didPop(route, previousRoute);
      stackCount--;
      debugPrint("didPop");
      routes.remove(route);
  }

  @override
  void didRemove(Route<dynamic> route, Route<dynamic> previousRoute) {
      super.didRemove(route, previousRoute);
      stackCount--;
      debugPrint("didRemove");
      routes.remove(route);
  }

  @override
  void didReplace({ Route<dynamic> oldRoute, Route<dynamic> newRoute }) {
      super.didReplace(oldRoute: oldRoute, newRoute: newRoute);
      debugPrint("didReplace");
      routes.remove(oldRoute);
      routes.add(newRoute);
  }
}
