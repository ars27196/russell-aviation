import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/repository/user_repository.dart';
import 'package:russellavaition/utils/helper/Localization.dart';

class DrawerWidget extends StatefulWidget {
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends StateMVC<DrawerWidget> {
  //ProfileController _con;

  /*_DrawerWidgetState() : super(ProfileController()) {
    //_con = controller;
  }*/

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          GestureDetector(
              onTap: () {
                print("${currentUser.value}");
                /*currentUser.value.apiToken != null
                  ? Navigator.of(context).pushNamed('/Profile')
                  : Navigator.of(context).pushNamed('/Login');*/
              },
              child: currentUser != null
                  ? UserAccountsDrawerHeader(
                      decoration: BoxDecoration(
                        color: Theme.of(context).hintColor.withOpacity(0.1),
                      ),
                      accountName: Text(
                          "${currentUser.value.firstName ?? ""} "+
                          "${currentUser
                          .value.lastName ?? ""}", //currentUser.value.name,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      accountEmail: Text(
                        currentUser.value.email ?? "",
                        style: Theme.of(context).textTheme.caption,
                      ),
                      currentAccountPicture: ClipOval(
                        child: Container(
                            color: Colors.white,
                            child: Image.asset(
                                "assets/images/user_image_placeholder.png")),
                      ))
                  : Container()),
          ListTile(
            onTap: () {
              Navigator.of(context).pushNamed('/Pages', arguments: 0);
            },
            leading: Icon(
              Icons.event_seat,
              color: Theme.of(context).focusColor.withOpacity(1),
            ),
            title: Text(
              Localization.stLocalized(
                  "seatBooking", Localization.isEnglishCode()),
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).pushNamed('/Pages', arguments: 1);
            },
            leading: Icon(
              Icons.local_airport,
              color: Theme.of(context).focusColor.withOpacity(1),
            ),
            title: Text(
              Localization.stLocalized("aboutUs", Localization.isEnglishCode()),
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).pushNamed('/Pages', arguments: 2);
            },
            leading: Icon(
              Icons.markunread_mailbox,
              color: Theme.of(context).focusColor.withOpacity(1),
            ),
            title: Text(
              Localization.stLocalized(
                  "contactUs", Localization.isEnglishCode()),
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).pushNamed('/Pages', arguments: 3);
            },
            leading: Icon(
              Icons.history,
              color: Theme.of(context).focusColor.withOpacity(1),
            ),
            title: Text(
              Localization.stLocalized(
                  "bookingHistory", Localization.isEnglishCode()),
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          ListTile(
            dense: true,
            title: Text(
              Localization.stLocalized(
                  "preferences", Localization.isEnglishCode()),
              style: Theme.of(context).textTheme.bodyText2,
            ),
            trailing: Icon(
              Icons.remove,
              color: Theme.of(context).focusColor.withOpacity(0.3),
            ),
          ),
          ListTile(
            onTap: () {
              if (currentUser.value.uId != null) {
                logout().then((value) {
                  Navigator.of(context).pushReplacementNamed("/Login");
                });
              } else {
                Navigator.of(context).pushReplacementNamed("/Login");
              }
            },
            leading: Icon(
              Icons.exit_to_app,
              color: Theme.of(context).focusColor.withOpacity(1),
            ),
            title: Text(
              Localization.stLocalized("logOut", Localization.isEnglishCode()),
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: ListTile(
              dense: true,
              title: Text(
                //S.of(context).version + " " + setting.value.appVersion,
                Localization.stLocalized(
                    "version", Localization.isEnglishCode()),
                style: Theme.of(context).textTheme.bodyText2,
              ),
              trailing: Icon(
                Icons.remove,
                color: Theme.of(context).focusColor.withOpacity(0.3),
              ),
            ),
          )
        ],
      ),
    );
  }
}
