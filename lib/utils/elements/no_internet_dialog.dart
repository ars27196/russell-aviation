import 'package:flutter/material.dart';

class NoInternetDialogAlert extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){},
      child: AlertDialog(
        title: Text("Information"),
        content: Text("Your device is not connected to Internet!"),
      ),
    );
  }
}
