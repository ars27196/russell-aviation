import 'package:flutter/material.dart';

//External
import 'package:flutter_spinkit/flutter_spinkit.dart';

// ignore: must_be_immutable
class ProgressHUD extends StatefulWidget {
  final Color backgroundColor;
  final Color color;
  final Color containerColor;
  final double borderRadius;
  final String text;
  final bool loading;
  final bool forInternalViews;
  _ProgressHUDState state;

  ProgressHUD(
      {Key key,
        this.backgroundColor = Colors.black54,
        this.color = Colors.white,
        this.containerColor = Colors.transparent,
        this.borderRadius = 10.0,
        this.text,
        this.loading = true, this.forInternalViews = false})
      : super(key: key);

  @override
  _ProgressHUDState createState() {
    state = new _ProgressHUDState();

    return state;
  }
}

class _ProgressHUDState extends State<ProgressHUD> {
  bool _visible = true;

  @override
  void initState() {
    super.initState();

    _visible = widget.loading;
  }

  void dismiss() {
    setState(() {
      this._visible = false;
    });
  }

  void show() {
    setState(() {
      this._visible = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_visible) {
      return new Scaffold(
          backgroundColor: widget.backgroundColor,
          body: new Stack(
            children: <Widget>[
              new Center(
                child: new Container(
                  width: widget.forInternalViews ? null : 100.0,
                  height: widget.forInternalViews ? null : 100.0,
                  decoration: new BoxDecoration(
                      color: widget.containerColor,
                      borderRadius: new BorderRadius.all(
                          new Radius.circular(widget.borderRadius))),
                  child: new Container(
                    margin: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                    child:
                    /*SizedBox(
                      width: 100.0,
                      child: TextLiquidFill(
                        text: Provider.of<Localization>(context).mtLocalized("appName"),
                        waveColor: EWPColors.arrowPagesPinColor,
                        waveDuration: Duration(milliseconds: 700),
                        boxBackgroundColor: EWPColors.themeColor,
                        textStyle: TextStyle(
                          fontSize: 60.0,
                          fontWeight: FontWeight.bold,
                        ),
                        boxHeight: 100.0,
                      ),
                    )*/
                    SpinKitRing(color: Colors.white, size: 50, lineWidth: 5, duration: Duration(milliseconds: 1000),),
                  ),
                ),
              ),
              widget.forInternalViews ? Center(
                child: _getCenterContent(),
              ) : Container()
            ],
          ));
    } else {
      return new Container();
    }
  }

  Widget _getCenterContent() {
    if (widget.text == null || widget.text.isEmpty) {
      return _getCircularProgress();
    }

    return new Center(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _getCircularProgress(),
          new Container(
            margin: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            child: SpinKitRing(color: Colors.white, size: 50, lineWidth: 5, duration: Duration(milliseconds: 1000),),
          )
        ],
      ),
    );
  }

  Widget _getCircularProgress() {
    return SpinKitRing(color: Colors.white, size: 50, lineWidth: 5, duration: Duration(milliseconds: 1000),);
  }
}
