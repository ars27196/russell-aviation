import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:russellavaition/utils/helper/Localization.dart';

class DateTimePicker extends StatefulWidget {
  final DateTime initialDateTime;
  final DateTime minimumDate;

  DateTimePicker({@required this.initialDateTime, this.minimumDate});

  @override
  State<StatefulWidget> createState() {
    return DateTimePickerState();
  }
}

class DateTimePickerState extends State<DateTimePicker> {
  DateTime selectedDate;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          PositionedDirectional(
              start: 0,
              end: 0,
              bottom: 0,
              //alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(0),
                    height: 50,
                    color: Theme.of(context).primaryColor,
                    child: Stack(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topLeft,
                          child: Divider(
                            color: Colors.white,
                            height: 1,
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Divider(
                            color: Colors.white,
                            height: 1,
                          ),
                        ),
                        Align(
                            alignment: Alignment.centerRight,
                            child: CupertinoButton(
                                child: Text(
                                    Localization.stLocalized(
                                        "done", Localization.isEnglishCode()),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .merge(TextStyle(fontSize: 17))),
                                onPressed: () {
                                  Navigator.of(context).pop(selectedDate);
                                })),
                        Align(
                            alignment: Alignment.centerLeft,
                            child: CupertinoButton(
                                child: Text(
                                    Localization.stLocalized(
                                        "cancel", Localization.isEnglishCode()),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .merge(TextStyle(fontSize: 17))),
                                onPressed: () {
                                  Navigator.of(context).pop(null);
                                })),
                      ],
                    ),
                  ),
                  Container(
                    height: 250,
                    child: CupertinoDatePicker(
                      mode: CupertinoDatePickerMode.dateAndTime,
                      initialDateTime: widget.initialDateTime,
                      backgroundColor: Theme.of(context).accentColor,
                      minimumDate: widget.minimumDate,
                      onDateTimeChanged: (dateTime) {
                        debugPrint("$dateTime");
                        selectedDate = dateTime;
                      },
                    ),
                  )
                ],
              )),
        ],
      ),
      backgroundColor: Colors.transparent,
    );
  }
}
