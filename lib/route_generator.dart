import 'package:flutter/material.dart';
import 'package:russellavaition/pages/admin/admin_booking_details_screen.dart';
import 'package:russellavaition/pages/admin/admin_pages.dart';
import 'package:russellavaition/pages/authentication/forget_password_screen.dart';
import 'package:russellavaition/pages/authentication/login_screen.dart';
import 'package:russellavaition/pages/authentication/signup_details_screen.dart';
import 'package:russellavaition/pages/authentication/signup_screen.dart';
import 'package:russellavaition/pages/bookingHistory/view_seat_booking_details_screen.dart';
import 'package:russellavaition/pages/pages.dart';
import 'package:russellavaition/pages/payment_screen.dart';
import 'package:russellavaition/pages/seatBooking/confirm_ticket_screen.dart';
import 'package:russellavaition/pages/splash_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    switch (settings.name) {
      case '/Splash':
        return MaterialPageRoute(builder: (_) => MySplashScreen());
      case '/Login':
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case '/SignUp':
        return MaterialPageRoute(builder: (_) => SignUpScreen());
      case '/ForgetPassword':
        return MaterialPageRoute(builder: (_) => ForgetPasswordScreen());
      case '/Pages':
        return MaterialPageRoute(builder: (_) => PagesScreen(currentTab: args));
      case '/AdminPages':
        return MaterialPageRoute(
            builder: (_) => AdminPagesScreen(currentTab: args));
      case '/SignUpDetails':
        return MaterialPageRoute(
            builder: (_) => SignUpDetailsScreen(argument: args));
      case '/ConfirmTicket':
        return MaterialPageRoute(
            builder: (_) => ConfirmSeatScreen(bookingData: args));
      case '/BookingDetail':
        return MaterialPageRoute(
            builder: (_) => SeatBookingDetailScreen(bookingData: args));
      case '/AdminBookingDetail':
        return MaterialPageRoute(
            builder: (_) => AdminSeatBookingDetailScreen(bookingData: args));
      case '/PaymentScreen':
        return MaterialPageRoute(builder: (_) => PaymentScreen(responseState: args));
      default:
        // If there is no such named route in the switch statement, e.g. /third
        return MaterialPageRoute(
            builder: (_) =>
                Scaffold(body: SafeArea(child: Text('Route Error'))));
    }
  }
}
