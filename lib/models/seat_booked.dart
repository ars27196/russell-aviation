import 'package:russellavaition/models/user.dart';

class SeatBookingModel {
  String oneWay;
  String departureDate;
  String arrivalDate;
  String departureFrom;
  String arrivalAt;
  String totalAmount;
  String advanceAmount;
  String noOfPassengers;
  bool isPaid;
  String bookingId;
  User user;

  SeatBookingModel(
      {this.bookingId,
      this.oneWay,
      this.departureDate,
      this.arrivalDate,
      this.departureFrom,
      this.arrivalAt,
      this.totalAmount,
      this.advanceAmount,
      this.noOfPassengers,
      this.isPaid = false,
      this.user});

  SeatBookingModel.fromJson(Map<String, dynamic> json) {
    bookingId = json['bookingId'];
    oneWay = json['oneway'];
    departureDate = json['departureDate'];
    arrivalDate = json['arrivalDate'];
    departureFrom = json['departureFrom'];
    arrivalAt = json['arrivalAt'];
    totalAmount = json['totalAmount'];
    advanceAmount = json['advanceAmount'];
    noOfPassengers = json['noOfPassengers'];
    isPaid = json['isPaid'];
    user = User.fromJSON(json['user']);
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bookingId'] = this.bookingId;
    data['oneway'] = this.oneWay;
    data['departureDate'] = this.departureDate;
    data['arrivalDate'] = this.arrivalDate;
    data['departureFrom'] = this.departureFrom;
    data['arrivalAt'] = this.arrivalAt;
    data['totalAmount'] = this.totalAmount;
    data['advanceAmount'] = this.advanceAmount;
    data["noOfPassengers"] = this.noOfPassengers;
    data["isPaid"] = this.isPaid;
    data["user"] = this.user.toMap();
    return data;
  }
}
