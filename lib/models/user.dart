class User {
  String uId;
  String firstName;
  String lastName;
  String email;
  String password;
  String phone;

  // used for indicate if client logged in or not
  bool auth;

//  String role;

  User({this.uId, this.email});
  User.fromJSON(Map<dynamic, dynamic> jsonMap) {
    try {
      uId = jsonMap['id'].toString();
      firstName = jsonMap['firstName'] != null ? jsonMap['firstName'] : '';
      lastName = jsonMap['lastName'] != null ? jsonMap['lastName'] : '';
      email = jsonMap['email'] != null ? jsonMap['email'] : '';
      try {
        phone = jsonMap['phone'];
      } catch (e) {
        phone = "";
      }

    } catch (e) {
      print("e : ${e.toString()}");
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = uId;
    map["email"] = email;
    map["firstName"] = firstName;
    map["lastName"] = lastName;
    map["phone"] = phone;
    return map;
  }

  @override
  String toString() {
    var map = this.toMap();
    map["auth"] = this.auth;
    return map.toString();
  }

  bool profileCompleted() {
    return  phone != null && phone != '';
  }
}
