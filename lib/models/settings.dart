import 'package:flutter/material.dart';

class AppSetting {
  String appName = '';
  String mainColor;
  String mainDarkColor;
  String secondColor;
  String secondDarkColor;
  String accentColor;
  String accentDarkColor;
  String scaffoldDarkColor;
  String scaffoldColor;

  ValueNotifier<Brightness> brightness = new ValueNotifier(Brightness.light);

  AppSetting();
}
