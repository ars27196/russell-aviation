import 'package:firebase_database/firebase_database.dart';
import 'package:russellavaition/models/seat_booked.dart';
import 'package:russellavaition/repository/user_repository.dart' as userRepo;

Future<dynamic> proceedToPayments(
    SeatBookingModel ticket, DatabaseReference reference) async {
  return reference
      .child("seatBooking")
      .child("/${userRepo.currentUser.value.uId}")
      .child("/${ticket.bookingId}")
      .set(ticket.toMap());
}
