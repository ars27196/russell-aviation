import 'dart:convert';

import 'package:http/http.dart' as http;

// sandbox url
// String baseUrl="https://russellaviationsquare-eg234cvcva-uc.a.run.app";
// production url
String baseUrl="https://in-app-payments-zifoviimoa-ue.a.run.app";

sendPaymentRequest(String nonce, int amount, completion(String response , bool success)) async {
  Map data = {'nonce': nonce, 'amount': amount};
  Map<String, String> headers = {'Content-Type': 'application/json'};
  final body = jsonEncode(data);
  var url =
      '$baseUrl/process-payment';
  await http.post(url, body: body, headers: headers).then((response) {
    print("Response status: ${response.statusCode}");
    print("Response body: ${response.body}");
    if(response.statusCode ==200){
      completion(response.body, true);
    }else{
      completion(response.body, false);
    }
  }).catchError((error) {
    print("Repo Errror: " + error.toString());
    completion(error.toString(), false);
  });
}
