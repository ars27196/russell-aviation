import 'dart:collection';

import 'package:firebase_database/firebase_database.dart';
import 'package:russellavaition/models/seat_booked.dart';
import 'package:russellavaition/repository/user_repository.dart' as userRepo;

void fetchListOfBookings(DatabaseReference reference,
    {void callback(List listOfBooking)}) async {
  List<SeatBookingModel> listOfSeats = [];
  print("/${userRepo.currentUser.value.uId}");
  await reference
      .child("seatBooking")
      .once()
      .then((DataSnapshot snapshot) {
    print("From history Repo: ");
    try {
      Map<dynamic, dynamic> map = snapshot.value;
      map.forEach((key, value) {
        try {
         value.forEach((key, value){
           SeatBookingModel seat =
           SeatBookingModel.fromJson(HashMap.from(value));
           listOfSeats.add(seat);
         });
        } catch (e) {
          print("e:$e");
        }
      });
      callback(listOfSeats);
    } catch (exception) {
      print("exception: $exception");
      callback(listOfSeats);
    }
  });
}
