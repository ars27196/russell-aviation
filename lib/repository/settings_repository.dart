import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:russellavaition/models/settings.dart';
import 'package:shared_preferences/shared_preferences.dart';

ValueNotifier<AppSetting> setting = new ValueNotifier(new AppSetting());
final navigatorKey = GlobalKey<NavigatorState>();
//LocationData locationData;

Future<AppSetting> initAppSettings() async {
  AppSetting _setting;
//  SharedPreferences prefs = await SharedPreferences.getInstance();
  _setting = new AppSetting();
  _setting.appName = "Russell Aviation";
  _setting.mainColor = "#000000";
  _setting.mainDarkColor = "#ffffff";
  _setting.secondColor = "#484848";
  _setting.secondDarkColor = "#ffffff";
  _setting.accentColor = "#000000";
  _setting.accentDarkColor = "#ffffff";
  _setting.scaffoldDarkColor = "#ffffff";
  _setting.scaffoldColor = "#ffffff";
  _setting.brightness.value = Brightness.dark;

  setting.value = _setting;
  setting.notifyListeners();

  /*final String url = '${GlobalConfiguration().getString('api_base_url')}settings';
  try {
    final response = await http.get(url, headers: {HttpHeaders.contentTypeHeader: 'application/json'});
    if (response.statusCode == 200 && response.headers.containsValue('application/json')) {
      if (json.decode(response.body)['data'] != null) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setString('settings', json.encode(json.decode(response.body)['data']));
       // _setting = AppSetting.fromJSON(json.decode(response.body)['data']);
        if (prefs.containsKey('language')) {
          _setting.mobileLanguage.value = Locale(prefs.get('language'), '');
        }
        _setting.brightness.value = prefs.getBool('isDark') ?? false ? Brightness.dark : Brightness.light;
        setting.value = _setting;
        // ignore: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member
        setting.notifyListeners();
      }
    } else {
      print(CustomTrace(StackTrace.current, message: response.body).toString());
    }
  } catch (e) {
    print(CustomTrace(StackTrace.current, message: url).toString());
   // return AppSetting.fromJSON({});
  }*/
  return setting.value;
}

void setBrightness(Brightness brightness) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (brightness == Brightness.dark) {
    prefs.setBool("isDark", true);
    brightness = Brightness.dark;
  } else {
    prefs.setBool("isDark", false);
    brightness = Brightness.light;
  }
}

Future<void> saveMessageId(String messageId) async {
  if (messageId != null) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('google.message_id', messageId);
  }
}

Future<String> getMessageId() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return await prefs.get('google.message_id');
}
