import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:square_in_app_payments/in_app_payments.dart';
import 'package:square_in_app_payments/models.dart';
import 'package:russellavaition/repository/payment_repository.dart' as repo;
import 'package:russellavaition/repository/user_repository.dart' as userRepo;

class PaymentController extends ControllerMVC {
  bool showStatusScreen = false;
  final String amount, bookingId;
  final GlobalKey<ScaffoldState> scaffoldKey;
  DatabaseReference dbRef;

  PaymentController(this.amount, this.scaffoldKey, this.bookingId) {
    initialize();
  }

  void initialize() {
    super.initState();
    print("payment Init");
    dbRef = FirebaseDatabase.instance.reference();
    _initSquarePayment().whenComplete(() => onStartCardEntryFlow());
  }

  Future<void> _initSquarePayment() async {
    //sandbox
    // await InAppPayments.setSquareApplicationId(
    //     'sandbox-sq0idb-CHWB2-WfW7gpyHEZWKEEUQ');
    // production
    await InAppPayments.setSquareApplicationId(
        'sq0idp-bca4f3iiLzT_t8S5XMzEVg');
  }

  /// An event listener to start card entry flow
  Future<void> onStartCardEntryFlow() async {
    await InAppPayments.startCardEntryFlow(
        onCardNonceRequestSuccess: _onCardEntryCardNonceRequestSuccess,
        onCardEntryCancel: _onCancelCardEntryFlow,
      collectPostalCode: false
    );
  }

  /// Callback when card entry is cancelled and UI is closed
  void _onCancelCardEntryFlow() {
    // Handle the cancel callback
  }

  /// Callback when successfully get the card nonce details for processig
  /// card entry is still open and waiting for processing card nonce details
  void _onCardEntryCardNonceRequestSuccess(CardDetails result) async {
    try {
      // take payment with the card nonce details
      // you can take a charge
      // await chargeCard(result);
      print("Nonce: ${result.nonce}");
      // payment finished successfully
      // you must call this method to close card entry
      repo.sendPaymentRequest(result.nonce, int.parse(amount),
          (response, success) {
        InAppPayments.completeCardEntry(
            onCardEntryComplete: _onCardEntryComplete);
        print(success);
        Navigator.of(scaffoldKey.currentContext)
            .pushReplacementNamed("/PaymentScreen", arguments: success);
        updatePaymentStatus();
      });
    } on Exception catch (ex) {
      InAppPayments.showCardNonceProcessingError(ex.toString() );
    }
  }

  /// Callback when the card entry is closed after call 'completeCardEntry'
  void _onCardEntryComplete() {
    // Update UI to notify user that the payment flow is finished successfully
  }

  void updatePaymentStatus() {
    dbRef
        .child("seatBooking")
        .child("/${userRepo.currentUser.value.uId}/$bookingId")
        .update({"isPaid": true});
  }
}
