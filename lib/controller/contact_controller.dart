import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/repository/contact_us_repository.dart' as repository;
class ContactUsScreenController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;
  String name;
  String subject;
  String message;

  GlobalKey<FormState> contactFormKey;

  ContactUsScreenController() {
    contactFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void sendMail() {
    FocusScope.of(context).unfocus();
    if (contactFormKey.currentState.validate()) {
      contactFormKey.currentState.save();
      repository.sendByEmail(body: message,subject: subject, name: name );
    }
  }
}
