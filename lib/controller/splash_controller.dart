import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/models/user.dart';

import '../repository/user_repository.dart' as userRepo;

class SplashScreenController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;

  SplashScreenController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  @override
  void initState() {
    super.initState();

    /*  Timer(Duration(seconds: 20), () {
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text("Verify You internet Connect Connection"),
      ));
    });*/
  }

  void loadUserData(BuildContext context) {
    userRepo.getCurrentUser().then((value) {
      if (value is User) {
        if (value.auth) {
          if (value.email == "ars27196@gmail.com") {
            Navigator.of(context)
                .pushReplacementNamed("/AdminPages", arguments: 0);
          } else {
            Navigator.of(context).pushReplacementNamed("/Pages", arguments: 0);
          }
        } else {
          Navigator.of(context).pushReplacementNamed("/Login");
        }
      } else {
        Navigator.of(context).pushReplacementNamed("/Login");
      }
    });
  }
}
