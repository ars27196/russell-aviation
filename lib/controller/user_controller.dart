import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/models/user.dart';
import 'package:russellavaition/pages/pages.dart';
import 'package:russellavaition/repository/user_repository.dart' as repo;
import 'package:russellavaition/services/auth.dart';
import 'package:russellavaition/utils/helper/Localization.dart';
import 'package:russellavaition/utils/helper/app_utils.dart';

class UserController extends ControllerMVC {
  User user = new User();
  bool hidePassword = true;
  bool loading = false;
  AuthService _auth;
  DatabaseReference dbRef;
  GlobalKey<FormState> loginFormKey;
  GlobalKey<FormState> registerFormKey;
  GlobalKey<FormState> registerDetailsFormKey;
  GlobalKey<FormState> forgotFormKey;
  GlobalKey<ScaffoldState> scaffoldKey;

  UserController() {
    loginFormKey = new GlobalKey<FormState>();
    registerFormKey = new GlobalKey<FormState>();
    registerDetailsFormKey = new GlobalKey<FormState>();
    forgotFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    _auth = AuthService();
    dbRef = FirebaseDatabase.instance.reference();
  }

  void login() async {
    FocusScope.of(context).unfocus();
    if (loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      AppUtils.showLoader(context: context);
      dynamic result = await _auth
          .signInWithEmailAndPassword(
              userEmail: user.email, userPassword: user.password)
          .whenComplete(() => AppUtils.dismissLoader(context: context))
          .catchError((onError) {
        print("Error on Login: ");
        print(onError);
      });
      if (result == null) {
        scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(Localization.stLocalized(
              "emailOrPassError", Localization.isEnglishCode())),
        ));
      } else {
        User user = result as User;

        if (AppUtils.adminEmailId(user.email)) {
          _updateCurrentUser(user.uId);
          Navigator.of(scaffoldKey.currentContext)
              .pushReplacementNamed('/AdminPages', arguments: 0);
        } else {
          _updateCurrentUser(user.uId);
          Navigator.of(scaffoldKey.currentContext)
              .pushReplacementNamed('/Pages', arguments: 0);
        }
      }
    }
  }

  void registerWithEmailPassword() async {
    FocusScope.of(context).unfocus();

    if (registerFormKey.currentState.validate()) {
      registerFormKey.currentState.save();
      AppUtils.showLoader(context: context);
      dynamic result = await _auth
          .registerUserWithEmailAndPassword(
              userEmail: user.email, userPassword: user.password)
          .then((result) {
            if (result is User) {
              User _user = result;
              _updateUserEmail(uId: _user.uId);
            }
          })
          .whenComplete(() => AppUtils.dismissLoader(context: context))
          .catchError((error) {
            scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text(Localization.stLocalized(
                    "emailOrPassError", Localization.isEnglishCode()))));
          });
    }
  }

  void _updateUserEmail({String uId}) async {
    await dbRef.child("user").child("/$uId").set(user.email).then((value) {
      print("help:: $user");
      user.uId = uId;
      Navigator.of(scaffoldKey.currentContext)
          .pushNamed('/SignUpDetails', arguments: user);
    });
  }

  void updateUserDetails() async {
    FocusScope.of(context).unfocus();

    if (registerDetailsFormKey.currentState.validate()) {
      registerDetailsFormKey.currentState.save();
      AppUtils.showLoader(context: context);
      await dbRef
          .child("user")
          .child("/${user.uId}")
          .set(user.toMap())
          .timeout(Duration(milliseconds: 15000))
          .then((value) {
        print("here $user");

        AppUtils.dismissLoader(context: context);
        repo.setCurrentUser(user);
        repo.currentUser.value.auth = true;
        Navigator.of(scaffoldKey.currentContext).push(MaterialPageRoute(
            builder: (route) => PagesScreen(
                  currentTab: 0,
                )));
      }).catchError((onError) {
        AppUtils.dismissLoader(context: context);
        print(onError.toString());
        scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(Localization.stLocalized(
              "uploadError", Localization.isEnglishCode())),
        ));
      });
    }
  }

  void resetPassword() async {
    FocusScope.of(context).unfocus();
    if (forgotFormKey.currentState.validate()) {
      forgotFormKey.currentState.save();

      await _auth
          .resetPassword(userEmail: user.email)
          .whenComplete(() => print("complete"))
          .catchError((onError) {
        print("Error on Login: " + onError.toString());
      });

      Navigator.of(scaffoldKey.currentContext)
          .pushReplacementNamed('/Login', arguments: 0);
    }
  }

  void _updateCurrentUser(String uId) {
    dbRef.child("user").child("/$uId").once().then((DataSnapshot snapshot) {
      try {
        User user = User.fromJSON(snapshot.value);
        user.auth = true;
        repo.currentUser.value = user;
        repo.setCurrentUser(user);
      } catch (e) {
        print("login Error: " + e.toString());
      }
    });
  }
}
