import 'package:flutter/material.dart';
import 'package:russellavaition/controller/payment_controller.dart';
import 'package:russellavaition/models/seat_booked.dart';
import 'package:russellavaition/repository/confirm_booking_repository.dart'
    as repo;
import 'package:firebase_database/firebase_database.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/utils/helper/Localization.dart';
import 'package:russellavaition/utils/helper/app_utils.dart';

class ConfirmSeatBookingScreenController extends ControllerMVC {
  DatabaseReference databaseReference;
  GlobalKey<ScaffoldState> scaffoldKey;

  @override
  void initState() {
    super.initState();
    databaseReference = FirebaseDatabase.instance.reference();
    scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void confirmTicket(SeatBookingModel ticket) async {
    FocusScope.of(context).unfocus();
    AppUtils.showLoader(context: context);
    ticket.bookingId=AppUtils.generateBookingId();
    dynamic result = repo
        .proceedToPayments(ticket, databaseReference)
        .whenComplete(() => AppUtils.dismissLoader(context: context));
    print("result Con: ${result}");
    if (result == null) {
      scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(Localization.stLocalized(
            "bookingError", Localization.isEnglishCode())),
      ));
    }
    else{
      print("Firebase Upload: success");
      new PaymentController(ticket.advanceAmount, scaffoldKey, ticket.bookingId);
    }
  }
}
