import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/models/seat_booked.dart';
import 'package:russellavaition/repository/admin_booking_history_repository.dart'
    as repo;
import 'package:russellavaition/repository/user_repository.dart' as userRepo;
import 'package:russellavaition/utils/helper/app_utils.dart';

class AdminBookingHistoryScreenController extends ControllerMVC {
  DatabaseReference databaseReference;
  GlobalKey<ScaffoldState> scaffoldKey;
  List<SeatBookingModel> listOfSeatBookingsHistory;

  @override
  void initState() {
    super.initState();
    databaseReference = FirebaseDatabase.instance.reference();
    scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void fetchAllBookings() async {
    FocusScope.of(context).unfocus();
    AppUtils.showLoader(context: context);
    repo.fetchListOfBookings(databaseReference, callback: (listOfBookings) {
      if (listOfBookings != null) {
        AppUtils.dismissLoader(context: context);
        listOfSeatBookingsHistory = listOfBookings;
        notifyListeners();
      }
    });
  }

  logOut() {
    if (userRepo.currentUser.value.uId != null) {
      userRepo.logout().then((value) {
        Navigator.of(context).pushNamed('/Login');
      });
    } else {
      Navigator.of(context).pushNamed('/Login');
    }
  }
}

