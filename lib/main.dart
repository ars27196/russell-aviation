import 'package:flutter/material.dart';
import 'package:russellavaition/utils/helper/connection_status_singleton.dart';
import 'models/settings.dart';
import 'route_generator.dart';
import 'utils/observer/navigator_observer.dart';
import 'utils/helper/app_config.dart' as config;
import 'repository/settings_repository.dart' as settingRepo;

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MainPageState createState() => new _MainPageState();
}

class _MainPageState extends State<MyApp> {
  @override
  void initState() {
    settingRepo.initAppSettings();
    //userRepo.getCurrentUser();
    ConnectionStatusSingleton connectionStatus = ConnectionStatusSingleton.getInstance();
    connectionStatus.initialize();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: settingRepo.setting,
      builder: (context, AppSetting _setting, _) {
        return MaterialApp(
            title: _setting.appName,
            navigatorObservers: [RNavigatorObserver()],
            initialRoute: '/Splash',
            onGenerateRoute: RouteGenerator.generateRoute,
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              fontFamily: 'Raleway',
              primaryColor: Color(0xFF252525),
              brightness: Brightness.dark,
              scaffoldBackgroundColor: Color(0xFF2C2C2C),
              accentColor: config.Colors().mainDarkColor(1),
              dividerColor: config.Colors().accentColor(0.1),
              hintColor: config.Colors().secondDarkColor(1),
              focusColor: config.Colors().accentDarkColor(1),
              textTheme: TextTheme(
                headline5: TextStyle(
                    fontSize: 20.0, color: config.Colors().secondDarkColor(1)),
                headline4: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.w600,
                    color: config.Colors().secondDarkColor(1)),
                headline3: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w600,
                    color: config.Colors().secondDarkColor(1)),
                headline2: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.w700,
                    color: config.Colors().mainDarkColor(1)),
                headline1: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.w300,
                    color: config.Colors().secondDarkColor(1)),
                subtitle1: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.w500,
                    color: config.Colors().secondDarkColor(1)),
                headline6: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.w600,
                    color: config.Colors().mainDarkColor(1)),
                bodyText2: TextStyle(
                    fontSize: 12.0, color: config.Colors().secondDarkColor(1)),
                bodyText1: TextStyle(
                    fontSize: 14.0, color: config.Colors().secondDarkColor(1)),
                caption: TextStyle(
                    fontSize: 12.0,
                    color: config.Colors().secondDarkColor(0.6)),
              ),
            ));
      },
    );
  }
}
