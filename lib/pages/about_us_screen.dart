import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:russellavaition/utils/helper/Localization.dart';

class AboutUsScreen extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  AboutUsScreen({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  AboutUsScreenState createState() => AboutUsScreenState();
}

class AboutUsScreenState extends State<AboutUsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          Localization.stLocalized("aboutUs", Localization.isEnglishCode())
              .toUpperCase(),
          style: Theme.of(context)
              .textTheme
              .headline6
              .merge(TextStyle(letterSpacing: 1.3)),
        ),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Image.asset("assets/images/plane_stock.jpg"),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                        Localization.stLocalized(
                            "aboutUsMessage", Localization.isEnglishCode()),
                        style: Theme.of(context).textTheme.bodyText1.merge(
                              TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontSize: 18),
                            )),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
