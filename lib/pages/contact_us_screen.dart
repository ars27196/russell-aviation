import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/controller/contact_controller.dart';
import 'package:russellavaition/utils/elements/BlockButtonWidget.dart';
import 'package:russellavaition/utils/helper/Localization.dart';

class ContactUsScreen extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  ContactUsScreen({this.parentScaffoldKey});

  @override
  State<StatefulWidget> createState() {
    return ContactUsScreenState();
  }
}

class ContactUsScreenState extends StateMVC<ContactUsScreen> {
  ContactUsScreenController _con;

  ContactUsScreenState() : super(ContactUsScreenController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          Localization.stLocalized("contactUs", Localization.isEnglishCode())
              .toUpperCase(),
          style: Theme.of(context)
              .textTheme
              .headline6
              .merge(TextStyle(letterSpacing: 1.3)),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                child: Image.asset(
                  "assets/images/contact_us.png",
                  color: Theme.of(context).accentColor,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 16, left: 10, right: 10),
                child: Form(
                  key: _con.contactFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(width: 8),
                          Icon(Icons.person_outline),
                          SizedBox(width: 8),
                          Text(
                            Localization.stLocalized(
                                    "name", Localization.isEnglishCode())
                                .toUpperCase(),
                            style: Theme.of(context)
                                .textTheme
                                .headline5
                                .merge(TextStyle(letterSpacing: 1.3)),
                          ),
                        ],
                      ),
                      SizedBox(height: 8),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        onSaved: (input) => _con.name = input,
                        validator: (input) => input.length < 3
                            ? Localization.stLocalized(
                                "moreThen3", Localization.isEnglishCode())
                            : null,
                        decoration: InputDecoration(
                          labelStyle:
                              TextStyle(color: Theme.of(context).accentColor),
                          contentPadding: EdgeInsets.all(12),
                          hintText: Localization.stLocalized(
                              "johnDoe", Localization.isEnglishCode()),
                          hintStyle: TextStyle(
                              color: Theme.of(context)
                                  .focusColor
                                  .withOpacity(0.7)),
                          errorStyle:
                              TextStyle(color: Color(0xFFff4545), fontSize: 16),
                        ),
                      ),
                      SizedBox(height: 8),
                      Row(
                        children: <Widget>[
                          SizedBox(width: 8),
                          Icon(Icons.subject),
                          SizedBox(width: 8),
                          Text(
                            Localization.stLocalized(
                                    "subject", Localization.isEnglishCode())
                                .toUpperCase(),
                            style: Theme.of(context)
                                .textTheme
                                .headline5
                                .merge(TextStyle(letterSpacing: 1.3)),
                          ),
                        ],
                      ),
                      SizedBox(height: 8),
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        onSaved: (input) => _con.subject = input,
                        validator: (input) => input.length < 3
                            ? Localization.stLocalized(
                                "moreThen3", Localization.isEnglishCode())
                            : null,
                        decoration: InputDecoration(
                          labelText: Localization.stLocalized(
                              "subject", Localization.isEnglishCode()),
                          labelStyle:
                              TextStyle(color: Theme.of(context).accentColor),
                          contentPadding: EdgeInsets.all(12),
                          hintText: 'Subject',
                          hintStyle: TextStyle(
                              color: Theme.of(context)
                                  .focusColor
                                  .withOpacity(0.7)),
                          errorStyle:
                              TextStyle(color: Color(0xFFff4545), fontSize: 16),
                        ),
                      ),
                      SizedBox(height: 8),
                      Row(
                        children: <Widget>[
                          SizedBox(width: 8),
                          Icon(Icons.message),
                          SizedBox(width: 8),
                          Text(
                            Localization.stLocalized(
                                    "message", Localization.isEnglishCode())
                                .toUpperCase(),
                            style: Theme.of(context)
                                .textTheme
                                .headline5
                                .merge(TextStyle(letterSpacing: 1.3)),
                          ),
                        ],
                      ),
                      SizedBox(height: 8),
                      TextFormField(
                        keyboardType: TextInputType.multiline,
                        onSaved: (input) => _con.message = input,
                        validator: (input) => input.length < 6
                            ? Localization.stLocalized(
                                "moreThen6", Localization.isEnglishCode())
                            : null,
                        maxLines: 4,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(12),
                          hintText: Localization.stLocalized(
                              "helpMessage", Localization.isEnglishCode()),
                          hintStyle: TextStyle(
                              color: Theme.of(context)
                                  .focusColor
                                  .withOpacity(0.7)),
                          errorStyle:
                              TextStyle(color: Color(0xFFff4545), fontSize: 16),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.2))),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.5))),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.5))),
                        ),
                      ),
                      SizedBox(height: 30),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: BlockButtonWidget(
                          text: Text(
                            Localization.stLocalized(
                                "sendMessage", Localization.isEnglishCode()),
                            style: TextStyle(
                                color: Theme.of(context).primaryColor),
                          ),
                          color: Colors.pinkAccent,
                          onPressed: () {
                            _con.sendMail();
                          },
                        ),
                      ),
                      SizedBox(height: 25),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
