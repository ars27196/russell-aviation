import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/controller/user_controller.dart';
import 'package:russellavaition/utils/elements/BlockButtonWidget.dart';
import 'package:russellavaition/utils/helper/Localization.dart';
import 'package:russellavaition/utils/helper/app_config.dart' as config;

import '../../repository/user_repository.dart' as userRepo;

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends StateMVC<LoginScreen> {
  UserController _con;

  LoginScreenState() : super(UserController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    if (userRepo.currentUser.value.auth != null) {
      print("init");
      if (userRepo.currentUser.value.auth) {
        print("init");

        Navigator.of(context).pushReplacementNamed('/Pages', arguments: 0 );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    print("build");

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _con.scaffoldKey,
        resizeToAvoidBottomPadding: false,
        body: SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Flexible(
                flex: 1,
                child: Stack(
                  fit: StackFit.expand,
                  alignment: AlignmentDirectional.topCenter,
                  children: <Widget>[
                    Container(height: 1),
                    Positioned(
                      top: 0,
                      child: Container(
                        width: config.App(context).appWidth(100),
                        height: config.App(context).appHeight(37),
                        decoration:
                            BoxDecoration(color: Theme.of(context).accentColor),
                      ),
                    ),
                    Positioned(
                      top: config.App(context).appHeight(37) - 120,
                      child: Container(
                        width: config.App(context).appWidth(84),
                        height: config.App(context).appHeight(37),
                        child: Text(
                          Localization.stLocalized(
                              "letsStartLogin", Localization.isEnglishCode()),
                          style: Theme.of(context).textTheme.headline2.merge(
                              TextStyle(color: Theme.of(context).primaryColor)),
                        ),
                      ),
                    ),
                    Positioned(
                      top: config.App(context).appHeight(37) - 50,
                      child: Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 50,
                                color:
                                    Theme.of(context).hintColor.withOpacity(0.2),
                              )
                            ]),
                        margin: EdgeInsets.symmetric(
                          horizontal: 20,
                        ),
                        padding: EdgeInsets.only(
                            top: 50, right: 27, left: 27, bottom: 20),
                        width: config.App(context).appWidth(88),
//              height: config.App(context).appHeight(55),
                        child: Form(
                          key: _con.loginFormKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              TextFormField(
                                keyboardType: TextInputType.emailAddress,
                                onSaved: (input) => _con.user.email = input,
                                validator: (input) => !input.contains('@')
                                    ? Localization.stLocalized("validEmail",
                                        Localization.isEnglishCode())
                                    : null,
                                decoration: InputDecoration(
                                  labelText: Localization.stLocalized(
                                      "email", Localization.isEnglishCode()),
                                  labelStyle: TextStyle(
                                      color: Theme.of(context).accentColor),
                                  contentPadding: EdgeInsets.all(12),
                                  hintText: 'johndoe@gmail.com',
                                  hintStyle: TextStyle(
                                      color: Theme.of(context)
                                          .focusColor
                                          .withOpacity(0.7)),
                                  errorStyle: TextStyle(
                                      color: Color(0xFFff4545), fontSize: 16),
                                  prefixIcon: Icon(Icons.alternate_email,
                                      color: Theme.of(context).accentColor),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.2))),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.5))),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.2))),
                                ),
                              ),
                              SizedBox(height: 30),
                              TextFormField(
                                keyboardType: TextInputType.text,
                                onSaved: (input) => _con.user.password = input,
                                validator: (input) => input.length < 3
                                    ? Localization.stLocalized(
                                        "moreThen3", Localization.isEnglishCode())
                                    : null,
                                obscureText: _con.hidePassword,
                                decoration: InputDecoration(
                                  labelText: Localization.stLocalized(
                                      "password", Localization.isEnglishCode()),
                                  labelStyle: TextStyle(
                                      color: Theme.of(context).accentColor),
                                  contentPadding: EdgeInsets.all(12),
                                  hintText: '••••••••••••',
                                  hintStyle: TextStyle(
                                      color: Theme.of(context)
                                          .focusColor
                                          .withOpacity(0.7)),
                                  errorStyle: TextStyle(
                                      color: Color(0xFFff4545), fontSize: 16),
                                  prefixIcon: Icon(Icons.lock_outline,
                                      color: Theme.of(context).accentColor),
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _con.hidePassword = !_con.hidePassword;
                                      });
                                    },
                                    color: Theme.of(context).focusColor,
                                    icon: Icon(_con.hidePassword
                                        ? Icons.visibility
                                        : Icons.visibility_off),
                                  ),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.2))),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.5))),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.2))),
                                ),
                              ),
                              SizedBox(height: 30),
                              BlockButtonWidget(
                                text: Text(
                                  Localization.stLocalized(
                                      "login", Localization.isEnglishCode()),
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor),
                                ),
                                color: Theme.of(context).accentColor,
                                onPressed: () {
                                  _con.login();
                                },
                              ),
                              SizedBox(height: 15),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 10,
                      child: Column(
                        children: <Widget>[
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pushNamed('/ForgetPassword');
                            },
                            textColor: Theme.of(context).hintColor,
                            child: Text(
                              Localization.stLocalized(
                                  "forgotPassword", Localization.isEnglishCode()),
                            ),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pushNamed('/SignUp');
                            },
                            textColor: Theme.of(context).hintColor,
                            child: Text(
                              Localization.stLocalized(
                                  "noAccount", Localization.isEnglishCode()),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
