import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/controller/user_controller.dart';
import 'package:russellavaition/models/user.dart';
import 'package:russellavaition/utils/elements/BlockButtonWidget.dart';
import 'package:russellavaition/utils/helper/Localization.dart';
import 'package:russellavaition/utils/helper/app_config.dart' as config;

// ignore: must_be_immutable
class SignUpDetailsScreen extends StatefulWidget {
  final dynamic argument;
  User  _user;

  SignUpDetailsScreen({Key key, this.argument}) {
    if (argument != null) {
      if (argument is User) {
        _user = argument;
      }
    }
  }

  @override
  _SignUpDetailsScreenState createState() => _SignUpDetailsScreenState(user : _user);
}

class _SignUpDetailsScreenState extends StateMVC<SignUpDetailsScreen> {
  UserController _con;
  _SignUpDetailsScreenState({ User user}) : super(UserController()) {
    _con = controller;
    _con.user=user;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _con.scaffoldKey,
        resizeToAvoidBottomPadding: false,
        body:
            Stack(alignment: AlignmentDirectional.topCenter, children: <Widget>[
          Positioned(
            top: 0,
            child: Container(
              width: config.App(context).appWidth(100),
              height: config.App(context).appHeight(29.5),
              decoration: BoxDecoration(color: Theme.of(context).accentColor),
            ),
          ),
          Positioned(
            top: config.App(context).appHeight(29.5) - 120,
            child: Container(
              width: config.App(context).appWidth(84),
              height: config.App(context).appHeight(29.5),
              child: Text(
                Localization.stLocalized(
                    "completeLogup", Localization.isEnglishCode()),
                style: Theme.of(context)
                    .textTheme
                    .headline2
                    .merge(TextStyle(color: Theme.of(context).primaryColor)),
              ),
            ),
          ),
          Positioned(
              top: config.App(context).appHeight(29.5) - 50,
              child: Container(
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 50,
                        color: Theme.of(context).hintColor.withOpacity(0.2),
                      )
                    ]),
                margin: EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                padding: EdgeInsets.symmetric(vertical: 50, horizontal: 27),
                width: config.App(context).appWidth(88),
//              height: config.App(context).appHeight(55),
                child: Form(
                    key: _con.registerDetailsFormKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        TextFormField(
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          onSaved: (input) => _con.user.firstName = input,
                          validator: (input) => input.length < 3
                              ? Localization.stLocalized(
                                  "moreThen3", Localization.isEnglishCode())
                              : null,
                          decoration: InputDecoration(
                            labelText: Localization.stLocalized(
                                "firstName", Localization.isEnglishCode()),
                            labelStyle:
                                TextStyle(color: Theme.of(context).accentColor),
                            contentPadding: EdgeInsets.all(12),
                            hintText: Localization.stLocalized(
                                "John", Localization.isEnglishCode()),
                            hintStyle: TextStyle(
                                color: Theme.of(context)
                                    .focusColor
                                    .withOpacity(0.7)),
                            errorStyle: TextStyle(
                                color: Color(0xFFff4545), fontSize: 16),
                            prefixIcon: Icon(Icons.person_outline,
                                color: Theme.of(context).accentColor),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.2))),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.5))),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.2))),
                          ),
                        ),
                        SizedBox(height: 30),
                        TextFormField(
                          keyboardType: TextInputType.text,
                          onSaved: (input) => _con.user.lastName = input,
                          validator: (input) => input.length < 3
                              ? Localization.stLocalized(
                              "moreThen3", Localization.isEnglishCode())
                              : null,
                          decoration: InputDecoration(
                            labelText: Localization.stLocalized(
                                "lastName", Localization.isEnglishCode()),
                            labelStyle:
                            TextStyle(color: Theme.of(context).accentColor),
                            contentPadding: EdgeInsets.all(12),
                            hintText: Localization.stLocalized(
                                "Doe", Localization.isEnglishCode()),
                            hintStyle: TextStyle(
                                color: Theme.of(context)
                                    .focusColor
                                    .withOpacity(0.7)),
                            errorStyle: TextStyle(
                                color: Color(0xFFff4545), fontSize: 16),
                            prefixIcon: Icon(Icons.person_outline,
                                color: Theme.of(context).accentColor),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.2))),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.5))),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.2))),
                          ),
                        ),
                        SizedBox(height: 30),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          onSaved: (input) => _con.user.phone = input,
                          validator: (input) => input.toString().replaceAll("-", "").length< 9
                              ? Localization.stLocalized(
                                  "validNumber", Localization.isEnglishCode())
                              : null,
                          decoration: InputDecoration(
                            labelText: Localization.stLocalized(
                                "phone", Localization.isEnglishCode()),
                            labelStyle:
                                TextStyle(color: Theme.of(context).accentColor),
                            contentPadding: EdgeInsets.all(12),
                            hintText: 'xxx-xxx-xxxx',
                            hintStyle: TextStyle(
                                color: Theme.of(context)
                                    .focusColor
                                    .withOpacity(0.7)),
                            errorStyle: TextStyle(
                                color: Color(0xFFff4545), fontSize: 16),
                            prefixIcon: Icon(Icons.phone,
                                color: Theme.of(context).accentColor),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.2))),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.5))),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.2))),
                          ),
                        ),

                        SizedBox(height: 30),
                        BlockButtonWidget(
                          text: Text(
                            Localization.stLocalized(
                                "register", Localization.isEnglishCode()),
                            style: TextStyle(
                                color: Theme.of(context).primaryColor),
                          ),
                          color: Theme.of(context).accentColor,
                          onPressed: () {
                            _con.updateUserDetails();
                          },
                        ),
                        SizedBox(height: 25),
                      ],
                    )),
              ))
        ]));
  }
}
