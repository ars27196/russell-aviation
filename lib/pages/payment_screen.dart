import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/utils/helper/Localization.dart';

class PaymentScreen extends StatefulWidget {
  final bool responseState;

  const PaymentScreen({Key key, this.responseState}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return PaymentScreenState();
  }
}

class PaymentScreenState extends StateMVC<PaymentScreen> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future<bool>.value(false),
      child: Scaffold(
        body: Column(
          children: <Widget>[
            Expanded(
                child: widget.responseState
                  ? Image.asset(
                      "assets/images/thumbsUp.jpg",
                        fit: BoxFit.fill,
                        width: double.infinity,
                    )
                  : Image.asset("assets/images/thumbsDown.jpg",
                      fit: BoxFit.fill, width: double.infinity),
            ),
            widget.responseState ? _successWidget() : _failureWidget()
          ],
        ),
      ),
    );
  }

  Widget _successWidget() {
    return Container(
      width: double.maxFinite,
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              Localization.stLocalized("success", Localization.isEnglishCode())
                  .toUpperCase(),
              style: TextStyle(
                color: Colors.green, //Theme.of(context).primaryColor,
                fontSize: 22,
              ),
            ),
          ),
          Padding(
            padding:
                EdgeInsets.only(top: 2.0, left: 8.0, bottom: 8.0, right: 5.0),
            child: Text(
              Localization.stLocalized(
                  "successMessage", Localization.isEnglishCode()),
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 16,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _circularButton(
              titleKey: "continue",
              onButtonPressed: () {
                Navigator.pushReplacementNamed(context, "/Pages", arguments: 0);
              },
            ),
          )
        ],
      ),
    );
  }

  Widget _failureWidget() {
    return Container(
      width: double.maxFinite,
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              Localization.stLocalized("failure", Localization.isEnglishCode())
                  .toUpperCase(),
              style: TextStyle(
                color: Colors.red, //Theme.of(context).primaryColor,
                fontSize: 22,
              ),
            ),
          ),
          Padding(
            padding:
                EdgeInsets.only(top: 2.0, left: 8.0, bottom: 8.0, right: 5.0),
            child: Text(
              Localization.stLocalized(
                  "failureMessage", Localization.isEnglishCode()),
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 16,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _circularButton(
              titleKey: "continue",
              onButtonPressed: () {
                Navigator.pushReplacementNamed(context, "/Pages", arguments: 0);
              },
            ),
          )
        ],
      ),
    );
  }

  Widget _circularButton(
      {@required String titleKey,
      Function onButtonPressed,
      double width = 160,
      Color buttonColor,
      Color textColor}) {
    return Container(
      width: width,
      padding: EdgeInsets.all(6.0),
      child: RaisedButton(
        color: buttonColor,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        onPressed: () {
          onButtonPressed();
        },
        child: Padding(
          padding: const EdgeInsets.all(14.0),
          child: Text(
            Localization.stLocalized(titleKey, Localization.isEnglishCode()),
            style: TextStyle(color: textColor),
          ),
        ),
      ),
    );
  }
}
