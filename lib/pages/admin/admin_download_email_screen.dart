import 'package:flutter/material.dart';

class AdminDownloadEmailScreen extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  AdminDownloadEmailScreen({this.parentScaffoldKey});
  @override
  State<StatefulWidget> createState() {
    return AdminDownloadEmailScreenState();
  }

}

class AdminDownloadEmailScreenState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("Admin Download Email")
              ],
            ),
          ),
        ),
      ),
    );
  }

}