import 'package:flutter/material.dart';
import 'package:russellavaition/models/route_argument.dart';
import 'package:russellavaition/pages/admin/admin_booking_history.dart';
import 'package:russellavaition/pages/admin/admin_download_email_screen.dart';
import 'package:russellavaition/pages/seatBooking/seat_booking_screen.dart';

class AdminPagesScreen extends StatefulWidget {
  dynamic currentTab;
  RouteArgument routeArgument;
  Widget currentPage = SeatBookingScreen();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  AdminPagesScreen({
    Key key,
    this.currentTab,
  }) {
    if (currentTab != null) {
      if (currentTab is RouteArgument) {
        routeArgument = currentTab;
        currentTab = int.parse(currentTab.uId);
      }
    } else {
      currentTab = 0;
    }
  }

  @override
  _AdminPagesScreenState createState() {
    return _AdminPagesScreenState();
  }
}

class _AdminPagesScreenState extends State<AdminPagesScreen> {
  initState() {
    super.initState();
    _selectTab(widget.currentTab);
  }

  @override
  void didUpdateWidget(AdminPagesScreen oldWidget) {
    _selectTab(oldWidget.currentTab);
    super.didUpdateWidget(oldWidget);
  }

  void _selectTab(int tabItem) {
    setState(() {
      widget.currentTab = tabItem;
      switch (tabItem) {
        case 0:
          widget.currentPage =
              AdminBookingHistoryScreen(parentScaffoldKey: widget.scaffoldKey);
          break;
        case 1:
          widget.currentPage =
              AdminDownloadEmailScreen(parentScaffoldKey: widget.scaffoldKey);
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: widget.scaffoldKey,
        body: widget.currentPage,
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedFontSize: 0,
          unselectedFontSize: 0,
          iconSize: 22,
          elevation: 0,
          backgroundColor: Colors.transparent,
          selectedIconTheme: IconThemeData(size: 28),
          unselectedItemColor: Theme.of(context).focusColor.withOpacity(1),
          currentIndex: widget.currentTab,
          onTap: (int i) {
            setState(() {
              this._selectTab(i);
            });
          },
          // this will be set when a new tab is tapped
          items: [
            BottomNavigationBarItem(
                title: new Container(height: 5.0),
                icon: widget.currentTab == 0
                    ? Container(
                        width: 42,
                        height: 42,
                        decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          borderRadius: BorderRadius.all(
                            Radius.circular(50),
                          ),
                          boxShadow: [
                            BoxShadow(
                                color: Theme.of(context)
                                    .accentColor
                                    .withOpacity(0.4),
                                blurRadius: 40,
                                offset: Offset(0, 15)),
                            BoxShadow(
                                color: Theme.of(context)
                                    .accentColor
                                    .withOpacity(0.4),
                                blurRadius: 13,
                                offset: Offset(0, 3))
                          ],
                        ),
                        child: new Icon(Icons.event_seat,
                            color: Theme.of(context).primaryColor),
                      )
                    : new Icon(Icons.event_seat)),
            BottomNavigationBarItem(
              icon: widget.currentTab == 1
                  ? Container(
                      width: 42,
                      height: 42,
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.all(
                          Radius.circular(50),
                        ),
                        boxShadow: [
                          BoxShadow(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.4),
                              blurRadius: 40,
                              offset: Offset(0, 15)),
                          BoxShadow(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.4),
                              blurRadius: 13,
                              offset: Offset(0, 3))
                        ],
                      ),
                      child: new Icon(Icons.local_airport,
                          color: Theme.of(context).primaryColor))
                  : Icon(Icons.local_airport),
              title: new Container(height: 0.0),
            ),
          ],
        ),
      ),
    );
  }
}
