import 'dart:async';

import 'package:flutter/material.dart';
import 'package:russellavaition/models/route_argument.dart';
import 'package:russellavaition/pages/about_us_screen.dart';
import 'package:russellavaition/pages/bookingHistory/booking_history_screen.dart';
import 'package:russellavaition/pages/contact_us_screen.dart';
import 'package:russellavaition/pages/seatBooking/seat_booking_data_class.dart';
import 'package:russellavaition/pages/seatBooking/seat_booking_screen.dart';
import 'package:russellavaition/utils/elements/DrawerWidget.dart';
import 'package:russellavaition/utils/elements/no_internet_dialog.dart';
import 'package:russellavaition/utils/helper/connection_status_singleton.dart';

// ignore: must_be_immutable
class PagesScreen extends StatefulWidget {
  dynamic currentTab;
  RouteArgument routeArgument;
  Widget currentPage = SeatBookingScreen();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  PagesScreen({
    Key key,
    this.currentTab,
  }) {
    if (currentTab != null) {
      if (currentTab is RouteArgument) {
        routeArgument = currentTab;
        currentTab = int.parse(currentTab.uId);
      }
    } else {
      currentTab = 0;
    }
  }

  @override
  PagesScreenState createState() {
    return PagesScreenState();
  }
}

class PagesScreenState extends State<PagesScreen> {

  StreamSubscription _connectionChangeStream;
  bool isAvailable;

  initState() {
    super.initState();
    _selectTab(widget.currentTab);
    ConnectionStatusSingleton connectionStatus = ConnectionStatusSingleton.getInstance();
    _connectionChangeStream = connectionStatus.connectionChange.listen(connectionChanged);

  }


  void connectionChanged(dynamic hasConnection) {
    setState(() {

      print(hasConnection);

      if(!hasConnection){
        showDialog(context: context,
        builder: (BuildContext context){
          return NoInternetDialogAlert();
        },
        barrierDismissible: false);
      }else{
        if(!isAvailable){
          Navigator.of(context).pop();
        }

      }
      isAvailable = hasConnection;
    });
  }

  @override
  void dispose() {
    print("dispose");
    _connectionChangeStream.cancel();
    super.dispose();
  }

  @override
  void didUpdateWidget(PagesScreen oldWidget) {
    _selectTab(oldWidget.currentTab);
    super.didUpdateWidget(oldWidget);
  }

  void _selectTab(int tabItem) {
    setState(() {
      widget.currentTab = tabItem;
      switch (tabItem) {
        case 0:
          widget.currentPage = SeatBookingScreen(
              parentScaffoldKey: widget.scaffoldKey,
              screenData: new SeatBookingData());
          break;
        case 1:
          widget.currentPage =
              AboutUsScreen(parentScaffoldKey: widget.scaffoldKey);
          break;
        case 2:
          widget.currentPage =
              ContactUsScreen(parentScaffoldKey: widget.scaffoldKey);
          break;
        case 3:
          widget.currentPage =
              SeatBookingHistoryScreen(parentScaffoldKey: widget.scaffoldKey);
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: widget.scaffoldKey,
        drawer: DrawerWidget(),
        /* endDrawer: FilterWidget(onFilter: (filter) {
           Navigator.of(context)
              .pushReplacementNamed('/Pages', arguments: widget.currentTab);
        }),*/
        body: widget.currentPage,
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedFontSize: 0,
          unselectedFontSize: 0,
          iconSize: 22,
          elevation: 0,
          backgroundColor: Colors.transparent,
          selectedIconTheme: IconThemeData(size: 28),
          unselectedItemColor: Theme.of(context).focusColor.withOpacity(1),
          currentIndex: widget.currentTab,
          onTap: (int i) {
            setState(() {
              this._selectTab(i);
            });
          },
          // this will be set when a new tab is tapped
          items: [
            BottomNavigationBarItem(
                title: new Container(height: 5.0),
                icon: widget.currentTab == 0
                    ? Container(
                        width: 42,
                        height: 42,
                        decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          borderRadius: BorderRadius.all(
                            Radius.circular(50),
                          ),
                          boxShadow: [
                            BoxShadow(
                                color: Theme.of(context)
                                    .accentColor
                                    .withOpacity(0.4),
                                blurRadius: 40,
                                offset: Offset(0, 15)),
                            BoxShadow(
                                color: Theme.of(context)
                                    .accentColor
                                    .withOpacity(0.4),
                                blurRadius: 13,
                                offset: Offset(0, 3))
                          ],
                        ),
                        child: new Icon(Icons.event_seat,
                            color: Theme.of(context).primaryColor),
                      )
                    : new Icon(Icons.event_seat)),
            BottomNavigationBarItem(
              icon: widget.currentTab == 1
                  ? Container(
                      width: 42,
                      height: 42,
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.all(
                          Radius.circular(50),
                        ),
                        boxShadow: [
                          BoxShadow(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.4),
                              blurRadius: 40,
                              offset: Offset(0, 15)),
                          BoxShadow(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.4),
                              blurRadius: 13,
                              offset: Offset(0, 3))
                        ],
                      ),
                      child: new Icon(Icons.local_airport,
                          color: Theme.of(context).primaryColor))
                  : Icon(Icons.local_airport),
              title: new Container(height: 0.0),
            ),
            BottomNavigationBarItem(
              icon: widget.currentTab == 2
                  ? Container(
                      width: 42,
                      height: 42,
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.all(
                          Radius.circular(50),
                        ),
                        boxShadow: [
                          BoxShadow(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.4),
                              blurRadius: 40,
                              offset: Offset(0, 15)),
                          BoxShadow(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.4),
                              blurRadius: 13,
                              offset: Offset(0, 3))
                        ],
                      ),
                      child: new Icon(Icons.markunread_mailbox,
                          color: Theme.of(context).primaryColor),
                    )
                  : Icon(Icons.markunread_mailbox),
              title: new Container(height: 0.0),
            ),
            BottomNavigationBarItem(
              icon: widget.currentTab == 3
                  ? Container(
                      width: 42,
                      height: 42,
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.all(
                          Radius.circular(50),
                        ),
                        boxShadow: [
                          BoxShadow(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.4),
                              blurRadius: 40,
                              offset: Offset(0, 15)),
                          BoxShadow(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.4),
                              blurRadius: 13,
                              offset: Offset(0, 3))
                        ],
                      ),
                      child: new Icon(Icons.history,
                          color: Theme.of(context).primaryColor),
                    )
                  : Icon(Icons.history),
              title: new Container(height: 0.0),
            ),
          ],
        ),
      ),
    );
  }



}
