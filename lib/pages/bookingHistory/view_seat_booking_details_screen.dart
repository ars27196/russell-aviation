import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/models/seat_booked.dart';
import 'package:russellavaition/utils/helper/Localization.dart';

class SeatBookingDetailScreen extends StatefulWidget {
  final SeatBookingModel bookingData;

  SeatBookingDetailScreen({this.bookingData});

  @override
  State<StatefulWidget> createState() {
    return SeatBookingDetailScreenState();
  }
}

class SeatBookingDetailScreenState extends StateMVC<SeatBookingDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          Localization.stLocalized(
                  "bookingSeatDetails", Localization.isEnglishCode())
              .toUpperCase(),
          style: Theme.of(context)
              .textTheme
              .headline6
              .merge(TextStyle(letterSpacing: 1.3)),
        ),
      ),
      body: Column(
        children: <Widget>[
          Card(
            margin: EdgeInsets.only(left: 20, right: 20),
            elevation: 20,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            color: Colors.white,
            child: Container(
              padding: EdgeInsets.only(left: 10, right: 10, bottom: 60),
              child: Column(
                children: <Widget>[
                  Container(
                    width: 250,
                    height: 180,
                    child: Image.asset(
                      "assets/images/app_logo.png",
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 4.0),
                          child: Text("Booking Id: ",
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .merge(TextStyle(
                                      color: Theme.of(context).primaryColor))),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 4.0),
                        child: Text(
                          widget.bookingData.bookingId,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .merge(TextStyle(color: Colors.blueAccent)),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Expanded(
                        child: Text(
                            Localization.stLocalized("departureDateTime",
                                Localization.isEnglishCode()),
                            style: Theme.of(context).textTheme.headline6.merge(
                                TextStyle(
                                    color: Theme.of(context).primaryColor))),
                      ),
                      Text(
                        widget.bookingData.departureDate,
                        style: Theme.of(context).textTheme.bodyText1.merge(
                            TextStyle(color: Theme.of(context).primaryColor)),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      widget.bookingData.oneWay == "true"
                          ? Container()
                          : Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Text(
                                    Localization.stLocalized("arrivalDateTime",
                                            Localization.isEnglishCode()) +
                                        ":",
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .merge(TextStyle(
                                            color: Theme.of(context)
                                                .primaryColor))),
                              ),
                            ),
                      widget.bookingData.oneWay == "true"
                          ? Container()
                          : Padding(
                              padding: const EdgeInsets.only(top: 10.0),
                              child: Text(widget.bookingData.arrivalDate,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      .merge(TextStyle(
                                          color:
                                              Theme.of(context).primaryColor))),
                            ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 4.0),
                          child: Text(
                              Localization.stLocalized("departureFrom",
                                      Localization.isEnglishCode()) +
                                  ":",
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .merge(TextStyle(
                                      color: Theme.of(context).primaryColor))),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Text(widget.bookingData.departureFrom,
                            style: Theme.of(context).textTheme.bodyText1.merge(
                                TextStyle(
                                    color: Theme.of(context).primaryColor))),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: Text(
                              Localization.stLocalized("arrivalAt",
                                      Localization.isEnglishCode()) +
                                  ":",
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .merge(TextStyle(
                                      color: Theme.of(context).primaryColor))),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Text(widget.bookingData.arrivalAt,
                            style: Theme.of(context).textTheme.bodyText1.merge(
                                TextStyle(
                                    color: Theme.of(context).primaryColor))),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(top: 10.0),
                          child: Text(
                              Localization.stLocalized("passengers",
                                      Localization.isEnglishCode()) +
                                  ":",
                              style: Theme.of(context).textTheme.headline6.merge(
                                  TextStyle(
                                      color: Theme.of(context).primaryColor))),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Text(widget.bookingData.noOfPassengers,
                            style: Theme.of(context).textTheme.bodyText1.merge(
                                TextStyle(
                                    color: Theme.of(context).primaryColor))),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(top: 10.0),
                          child: Text(
                              Localization.stLocalized("totalAmount",
                                      Localization.isEnglishCode()) +
                                  ":",
                              style: Theme.of(context).textTheme.headline6.merge(
                                  TextStyle(
                                      color: Theme.of(context).primaryColor))),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Text(widget.bookingData.totalAmount,
                            style: Theme.of(context).textTheme.bodyText1.merge(
                                TextStyle(
                                    color: Theme.of(context).primaryColor))),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: Text(
                              Localization.stLocalized("advanceAmount",
                                      Localization.isEnglishCode()) +
                                  ":",
                              style: Theme.of(context).textTheme.headline6.merge(
                                  TextStyle(
                                      color: Theme.of(context).primaryColor))),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Text(widget.bookingData.advanceAmount,
                            style: Theme.of(context).textTheme.bodyText1.merge(
                                TextStyle(
                                    color: Theme.of(context).primaryColor))),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: Text(
                              Localization.stLocalized(
                                      "status", Localization.isEnglishCode()) +
                                  ":",
                              style: Theme.of(context).textTheme.headline6.merge(
                                  TextStyle(
                                      color: Theme.of(context).primaryColor))),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Text(
                            widget.bookingData.isPaid ? "Paid" : "Unpaid",
                            style: widget.bookingData.isPaid
                                ? Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .merge(TextStyle(color: Colors.lightGreen))
                                : Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .merge(TextStyle(color: Colors.redAccent))),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
