import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/controller/seat_booking_history_controller.dart';
import 'package:russellavaition/utils/helper/Localization.dart';

class SeatBookingHistoryScreen extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  SeatBookingHistoryScreen({this.parentScaffoldKey});

  @override
  State<StatefulWidget> createState() {
    return SeatBookingHistoryScreenState();
  }
}

class SeatBookingHistoryScreenState extends StateMVC<SeatBookingHistoryScreen> {
  SeatBookingHistoryScreenController _con;

  SeatBookingHistoryScreenState()
      : super(SeatBookingHistoryScreenController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _con.fetchAllBookings();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          Localization.stLocalized(
                  "bookingHistoryTitle", Localization.isEnglishCode())
              .toUpperCase(),
          style: Theme.of(context)
              .textTheme
              .headline6
              .merge(TextStyle(letterSpacing: 1.3)),
        ),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                  itemCount: _con.listOfSeatBookingsHistory == null
                      ? 0
                      : _con.listOfSeatBookingsHistory.isEmpty? 1: _con.listOfSeatBookingsHistory.length,
                  shrinkWrap: true,
                  itemBuilder: (context, position) {
                    return _con.listOfSeatBookingsHistory.length == 0
                        ? _emptyCardWidget()
                        : _historyCardWidget(index: position);
                  }),
            )
          ],
        ),
      ),
    );
  }

  Widget _emptyCardWidget() {
    return Container(
      padding: EdgeInsets.all(20.0),
      margin: EdgeInsets.all(20.0),
      color: Colors.white,
      child: Center(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: Image.asset("assets/images/nothing_found.png"),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                  Localization.stLocalized(
                      "noHistoryFound", Localization.isEnglishCode()),
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .merge(TextStyle(color: Theme.of(context).primaryColor))),
            ),
          ],
        ),
      ),
    );
  }

  Widget _historyCardWidget({int index}) {
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10),
      child: GestureDetector(
        child: Card(
          elevation: 20,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RichText(
                    text: TextSpan(children: [
                      TextSpan(
                        text: "Booking Id: ",
                        style: Theme.of(context).textTheme.bodyText1.merge(
                            TextStyle(color: Theme.of(context).primaryColor)),
                      ),
                      TextSpan(
                        text: _con.listOfSeatBookingsHistory[index].bookingId,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1
                            .merge(TextStyle(color: Colors.lightBlueAccent)),
                      )
                    ]),
                  )),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      Localization.stLocalized(
                          "departureFrom", Localization.isEnglishCode()),
                      style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(color: Theme.of(context).primaryColor)),
                    ),
                    Text(
                      _con.listOfSeatBookingsHistory[index].departureFrom
                          .replaceAll("\n", " "),
                      style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(color: Theme.of(context).primaryColor)),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      Localization.stLocalized(
                          "arrivalAt", Localization.isEnglishCode()),
                      style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(color: Theme.of(context).primaryColor)),
                    ),
                    Text(
                      _con.listOfSeatBookingsHistory[index].arrivalAt
                          .replaceAll("\n", " "),
                      style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(color: Theme.of(context).primaryColor)),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      Localization.stLocalized(
                          "passengers", Localization.isEnglishCode()),
                      style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(color: Theme.of(context).primaryColor)),
                    ),
                    Text(
                      _con.listOfSeatBookingsHistory[index].noOfPassengers,
                      style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(color: Theme.of(context).primaryColor)),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      Localization.stLocalized(
                          "departureDateTime", Localization.isEnglishCode()),
                      style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(color: Theme.of(context).primaryColor)),
                    ),
                    Text(
                      _con.listOfSeatBookingsHistory[index].departureDate,
                      style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(color: Theme.of(context).primaryColor)),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Center(
                  child: Text(
                    "Click to View Details",
                    style: Theme.of(context).textTheme.bodyText1.merge(
                        TextStyle(color: Colors.teal),
                    ),

                  ),
                ),
              )
            ],
          ),
        ),
        onTap: () {
          Navigator.of(context).pushNamed("/BookingDetail",
              arguments: _con.listOfSeatBookingsHistory[index]);
        },
      ),
    );
  }
}
