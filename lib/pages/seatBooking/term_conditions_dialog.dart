import 'package:flutter/material.dart';
import 'package:russellavaition/models/seat_booked.dart';
import 'package:russellavaition/utils/helper/Localization.dart';

class TermsCondition extends StatefulWidget {
  final SeatBookingModel seatBookingModel;

  TermsCondition(this.seatBookingModel);

  @override
  State<StatefulWidget> createState() {
    return TermsConditionState();
  }
}

class TermsConditionState extends State<TermsCondition> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SafeArea(
        bottom: false,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(10),
            color: Colors.white,
            margin: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Terms & Conditions",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                  ),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Text(
                          Localization.stLocalized(
                              "term_condition", Localization.isEnglishCode()),
                          style: TextStyle(color: Colors.black, fontSize: 14),
                        ))
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: _circularButton(
                    titleKey: "agree",
                    width: double.maxFinite,
                    buttonColor: Colors.pinkAccent,
                    textColor: Colors.white,
                    onButtonPressed: () {
                      setState(() {
                        // show term & conditions dialog
                        Navigator.pop(context);
                        Navigator.pushNamed(context, "/ConfirmTicket", arguments: widget.seatBookingModel);
                      });
                    },
                  ),
                ),
              ]
            ),

          ),
        ),
      ),
    );
  }

  Widget _circularButton(
      {String titleKey,
      Function onButtonPressed,
      double width = 160,
      Color buttonColor,
      Color textColor}) {
    return Container(
      width: width,
      padding: EdgeInsets.all(6.0),
      child: RaisedButton(
        color: buttonColor,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        onPressed: () {
          onButtonPressed();
        },
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            Localization.stLocalized(titleKey, Localization.isEnglishCode()),
            style: TextStyle(color: textColor),
          ),
        ),
      ),
    );
  }
}
