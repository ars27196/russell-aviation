import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/models/seat_booked.dart';
import 'package:russellavaition/pages/seatBooking/seat_booking_data_class.dart';
import 'package:russellavaition/pages/seatBooking/term_conditions_dialog.dart';
import 'package:russellavaition/utils/elements/date_picker.dart';
import 'package:russellavaition/utils/helper/Localization.dart';
import 'package:russellavaition/utils/helper/enums.dart';
import 'package:wheel_chooser/wheel_chooser.dart';
import 'package:russellavaition/repository/user_repository.dart' as repo;

class SeatBookingScreen extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;
  final SeatBookingData screenData;

  SeatBookingScreen({this.parentScaffoldKey, this.screenData});

  @override
  State<StatefulWidget> createState() {
    return SeatBookingScreenState();
  }
}

class SeatBookingScreenState extends StateMVC<SeatBookingScreen> {
  DateTime todayDate = DateTime.now();
  var scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          Localization.stLocalized("seatBooking", Localization.isEnglishCode())
              .toUpperCase(),
          style: Theme.of(context)
              .textTheme
              .headline6
              .merge(TextStyle(letterSpacing: 1.3)),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(children: <Widget>[
            Container(
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 8.0, bottom: 8.0, left: 22, right: 22),
                child: Image.asset(
                  "assets/images/plane.png",
                  color: Theme.of(context).accentColor,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 6, right: 6),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                          Localization.stLocalized(
                                  "departureFrom", Localization.isEnglishCode())
                              .toUpperCase(),
                          style: Theme.of(context).textTheme.headline3.merge(
                                TextStyle(letterSpacing: 1.3),
                              )),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        Localization.stLocalized(
                                "arrivalAt", Localization.isEnglishCode())
                            .toUpperCase(),
                        style: Theme.of(context).textTheme.headline3.merge(
                              TextStyle(letterSpacing: 1.3),
                            )),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 6, right: 6),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                          Localization.stLocalized(
                                  widget.screenData.isAddress2
                                      ? "airport1"
                                      : "airport2",
                                  Localization.isEnglishCode())
                              .toUpperCase(),
                          style: Theme.of(context).textTheme.headline5.merge(
                                TextStyle(letterSpacing: 1.3),
                              )),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        Localization.stLocalized(
                                widget.screenData.isAddress2
                                    ? "airport2"
                                    : "airport1",
                                Localization.isEnglishCode())
                            .toUpperCase(),
                        style: Theme.of(context).textTheme.headline5.merge(
                              TextStyle(letterSpacing: 1.3),
                            ),
                        textAlign: TextAlign.end,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 6, right: 6),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        Localization.stLocalized(
                                "city", Localization.isEnglishCode())
                            .toUpperCase(),
                        style: Theme.of(context).textTheme.headline5.merge(
                              TextStyle(letterSpacing: 1.3),
                            )),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        Localization.stLocalized(
                                "city", Localization.isEnglishCode())
                            .toUpperCase(),
                        style: Theme.of(context).textTheme.headline5.merge(
                              TextStyle(letterSpacing: 1.3),
                            )),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: GestureDetector(
                child: Image.asset(
                  "assets/images/reverse_icon.png",
                  color: Theme.of(context).accentColor,
                ),
                onTap: () {
                  setState(() {
                    if (widget.screenData.isAddress2) {
                      widget.screenData.isAddress2 = false;
                    } else {
                      widget.screenData.isAddress2 = true;
                    }
                  });
                },
              ),
            ),
            _segmentedControlButtonWidget(),
            _selectDateTimeWidget(context),
            _passengerSelectWidget(),
            _costEstimationWidget(),
            Padding(
              padding: const EdgeInsets.only(
                  top: 8.0, left: 20, right: 20, bottom: 20),
              child: _circularButton(
                titleKey: "proceed",
                width: double.maxFinite,
                buttonColor: Colors.pinkAccent,
                textColor: Colors.white,
                onButtonPressed: () {
                  setState(() {
                    //show term & conditions dialog
                    if (widget.screenData.isOneWay) {
                      if (widget.screenData.departureDate != null) {
                        // show dialog
                        var seatBookingModel = SeatBookingModel(
                            oneWay: widget.screenData.isOneWay.toString(),
                            departureDate:
                                widget.screenData.departureDateFormated,
                            totalAmount: widget.screenData.isOneWay
                                ? widget.screenData.seatPriceOneWayTotal
                                    .toString()
                                : widget.screenData.seatPriceTwoWayTotal
                                    .toString(),
                            advanceAmount: widget.screenData.isOneWay
                                ? widget.screenData.advanceAmountOneWayTotal
                                    .toString()
                                : widget.screenData.advanceAmountTwoWayTotal
                                    .toString(),
                            noOfPassengers:
                                widget.screenData.noOfPassenger.toString(),
                            departureFrom: widget.screenData.isAddress2
                                ? widget.screenData.airport1
                                : widget.screenData.airport2,
                            arrivalAt: widget.screenData.isAddress2
                                ? widget.screenData.airport2
                                : widget.screenData.airport1,
                            user: repo.currentUser.value);
                        print("check: ${widget.screenData.isOneWay}");
                        showDialog(
                          context: context,
                          builder: (_) => TermsCondition(seatBookingModel),
                        );
                      } else {
                        // error message
                        scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text(Localization.stLocalized(
                              "selectDateError", Localization.isEnglishCode())),
                        ));
                      }
                    } else {
                      if (widget.screenData.arrivalDate != null &&
                          widget.screenData.departureDate != null) {
                        // show dialog
                        var seatBookingModel = SeatBookingModel(
                            oneWay: widget.screenData.isOneWay.toString(),
                            departureDate:
                                widget.screenData.departureDateFormated,
                            arrivalDate: widget.screenData.arrivalDateFormated,
                            totalAmount: widget.screenData.isOneWay
                                ? widget.screenData.seatPriceOneWayTotal
                                    .toString()
                                : widget.screenData.seatPriceTwoWayTotal
                                    .toString(),
                            advanceAmount: widget.screenData.isOneWay
                                ? widget.screenData.advanceAmountOneWayTotal
                                    .toString()
                                : widget.screenData.advanceAmountTwoWayTotal
                                    .toString(),
                            noOfPassengers:
                                widget.screenData.noOfPassenger.toString(),
                            departureFrom: widget.screenData.isAddress2
                                ? widget.screenData.airport1
                                : widget.screenData.airport2,
                            arrivalAt: widget.screenData.isAddress2
                                ? widget.screenData.airport2
                                : widget.screenData.airport1,
                            user: repo.currentUser.value);
                        print("check: ${widget.screenData.isOneWay}");
                        showDialog(
                          context: context,
                          builder: (_) => TermsCondition(seatBookingModel),
                        );
                      } else {
                        // show error message
                        scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text(Localization.stLocalized(
                              "selectDateError", Localization.isEnglishCode())),
                        ));
                      }
                    }
                  });
                },
              ),
            ),
          ]),
        ),
      ),
    );
  }

  Widget _costEstimationWidget() {
    return (Container(
      margin: EdgeInsets.all(20),
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "Seat Price",
                style: Theme.of(context)
                    .textTheme
                    .headline2
                    .merge(TextStyle(color: Theme.of(context).primaryColor)),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      "\$${widget.screenData.isOneWay ? widget.screenData.seatPriceOneWay : widget.screenData.seatPriceTwoWay}",
                      style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 18)),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 02),
                      child: Text(
                        "x${widget.screenData.noOfPassenger}",
                        style: Theme.of(context).textTheme.bodyText1.merge(
                            TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 18)),
                      ),
                    ),
                  ],
                ),
                Text(
                  "\$${widget.screenData.isOneWay ? widget.screenData.seatPriceOneWayTotal : widget.screenData.seatPriceTwoWayTotal}",
                  style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 18)),
                ),
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Text(
                "Amount Advanced",
                style: Theme.of(context)
                    .textTheme
                    .headline2
                    .merge(TextStyle(color: Theme.of(context).primaryColor)),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      "\$${widget.screenData.isOneWay ? widget.screenData.advanceAmountOneWay : widget.screenData.advanceAmountTwoWay}",
                      style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 18)),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 02),
                      child: Text(
                        "x${widget.screenData.noOfPassenger}",
                        style: Theme.of(context).textTheme.bodyText1.merge(
                            TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 18)),
                      ),
                    ),
                  ],
                ),
                Text(
                  "\$${widget.screenData.isOneWay ? widget.screenData.advanceAmountOneWayTotal : widget.screenData.advanceAmountTwoWayTotal}",
                  style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 18)),
                ),
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Text(
                "Amount Due",
                style: Theme.of(context)
                    .textTheme
                    .headline2
                    .merge(TextStyle(color: Theme.of(context).primaryColor)),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      "\$${widget.screenData.isOneWay ? widget.screenData.amountDueOneWay : widget.screenData.amountDueTwoWayTotal}",
                      style: Theme.of(context).textTheme.bodyText1.merge(
                          TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 18)),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 02),
                      child: Text(
                        "x${widget.screenData.noOfPassenger}",
                        style: Theme.of(context).textTheme.bodyText1.merge(
                            TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 18)),
                      ),
                    ),
                  ],
                ),
                Text(
                  "\$${widget.screenData.isOneWay ? widget.screenData.amountDueOneWayTotal : widget.screenData.amountDueTwoWayTotal}",
                  style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 18)),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Text(
                    "Amount Playable",
                    style: Theme.of(context).textTheme.headline2.merge(
                        TextStyle(color: Theme.of(context).primaryColor)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Text(
                    "\$${widget.screenData.isOneWay ? widget.screenData.advanceAmountOneWayTotal : widget.screenData.advanceAmountTwoWayTotal}",
                    style: Theme.of(context).textTheme.bodyText1.merge(
                        TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 18,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      decoration: BoxDecoration(color: Theme.of(context).accentColor),
    ));
  }

  Widget _passengerSelectWidget() {
    return Container(
      margin: EdgeInsets.all(20),
      padding: EdgeInsets.all(20),
//      width: 343,
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child:
                Icon(Icons.event_seat, color: Theme.of(context).primaryColor),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: 2,
              height: 60,
              color: Colors.black,
            ),
          ),
          Flexible(
            flex: 1,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Text(
                      "Passengers",
                      style: Theme.of(context).textTheme.headline2.merge(
                          TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 18)),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: WheelChooser.integer(
                      onValueChanged: (s) {
                        setState(() {
                          widget.screenData.noOfPassenger = s as int;
                          widget.screenData.calculateFare();
                        });
                      },
                      maxValue: 6,
                      minValue: 1,
                      listWidth: 40,
                      initValue: 1,
                      horizontal: true,
                      selectTextStyle: TextStyle(
                          color: Colors.pinkAccent,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                      unSelectTextStyle: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Theme.of(context).primaryColor,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                  ),
                ]),
          )
        ],
      ),
      decoration: BoxDecoration(color: Theme.of(context).accentColor),
    );
  }

  Widget _selectDateTimeWidget(BuildContext context) {
    return Column(
      children: <Widget>[
        GestureDetector(
          child: Container(
            margin: EdgeInsets.all(20),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(color: Theme.of(context).accentColor),
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child:
                      Icon(Icons.today, color: Theme.of(context).primaryColor),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: 2,
                    height: 40,
                    color: Colors.black,
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Text(
                          Localization.stLocalized(
                              "select", Localization.isEnglishCode()),
                          style: Theme.of(context).textTheme.headline2.merge(
                              TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 18)),
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8.0, top: 2, bottom: 4),
                        child: Text(
                          widget.screenData.departureDate != null
                              ? widget.screenData.departureDateFormated
                              : Localization.stLocalized("departureDate",
                                  Localization.isEnglishCode()),
                          style: TextStyle(
                              fontSize: 18,
                              color: Theme.of(context).primaryColor),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          onTap: () {
            _dateTimePicker(type: DatePickerType.Departure);
          },
        ),
        widget.screenData.isOneWay
            ? Container()
            : GestureDetector(
                child: Container(
                  margin: EdgeInsets.all(20),
                  padding: EdgeInsets.all(20),
                  decoration:
                      BoxDecoration(color: Theme.of(context).accentColor),
                  child: GestureDetector(
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(Icons.today,
                              color: Theme.of(context).primaryColor),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: 2,
                            height: 40,
                            color: Colors.black,
                          ),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left: 10.0),
                                child: Text(
                                  Localization.stLocalized(
                                      "select", Localization.isEnglishCode()),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline2
                                      .merge(TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontSize: 18)),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 8.0, top: 2, bottom: 4),
                                child: Text(
                                  widget.screenData.arrivalDate != null
                                      ? widget.screenData.arrivalDateFormated
                                      : Localization.stLocalized("arrivalDate",
                                          Localization.isEnglishCode()),
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Theme.of(context).primaryColor),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    onTap: () {
                      _dateTimePicker(type: DatePickerType.Arrival);
                    },
                  ),
                ),
                onTap: () {
                  _dateTimePicker(type: DatePickerType.Arrival);
                },
              )
      ],
    );
  }

  Widget _segmentedControlButtonWidget() {
    return Container(
      margin: EdgeInsets.only(top: 20, left: 30, right: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: _circularButton(
              titleKey: "oneWay",
              buttonColor: widget.screenData.isOneWay
                  ? Colors.pinkAccent
                  : Theme.of(context).accentColor,
              textColor: widget.screenData.isOneWay
                  ? Colors.white
                  : Theme.of(context).primaryColor,
              onButtonPressed: () {
                setState(() {
                  if (!widget.screenData.isOneWay) {
                    widget.screenData.isOneWay = true;
                    widget.screenData.calculateFare();
                  }
                });
              },
            ),
          ),
          Expanded(
            child: _circularButton(
              titleKey: "twoWay",
              buttonColor: !widget.screenData.isOneWay
                  ? Colors.pinkAccent
                  : Theme.of(context).accentColor,
              textColor: !widget.screenData.isOneWay
                  ? Colors.white
                  : Theme.of(context).primaryColor,
              onButtonPressed: () {
                setState(() {
                  if (widget.screenData.isOneWay) {
                    widget.screenData.isOneWay = false;
                    widget.screenData.calculateFare();
                  }
                });
              },
            ),
          )
        ],
      ),
      decoration: BoxDecoration(
          border: Border.all(
            color: Theme.of(context).accentColor,
          ),
          borderRadius: BorderRadius.all(Radius.circular(20))),
    );
  }

  Widget _circularButton(
      {String titleKey,
      Function onButtonPressed,
      double width = 160,
      Color buttonColor,
      Color textColor}) {
    return Container(
      width: width,
      padding: EdgeInsets.all(6.0),
      child: RaisedButton(
        color: buttonColor,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        onPressed: () {
          onButtonPressed();
        },
        child: Padding(
          padding: const EdgeInsets.all(14.0),
          child: Text(
            Localization.stLocalized(titleKey, Localization.isEnglishCode()),
            style: TextStyle(color: textColor),
          ),
        ),
      ),
    );
  }

  _dateTimePicker({@required DatePickerType type}) {
    return showCupertinoDialog(
        context: context,
        builder: (context) {
          return DateTimePicker(
            initialDateTime: todayDate,
          );
        }).then((date) {
      var selectedDate;
      date != null ? selectedDate = date : selectedDate = DateTime.now();
      updateSelectedDate(selectedDate, type);
    });
  }

  void updateSelectedDate(date, type) {
    setState(() {
      if (type == DatePickerType.Arrival) {
        widget.screenData.arrivalDate = date as DateTime;
      }
      if (type == DatePickerType.Departure) {
        widget.screenData.departureDate = date as DateTime;
      }
    });
  }
}
