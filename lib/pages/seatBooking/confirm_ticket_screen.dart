import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:russellavaition/controller/confirm_seat_controller.dart';
import 'package:russellavaition/models/seat_booked.dart';
import 'package:russellavaition/utils/helper/Localization.dart';

class ConfirmSeatScreen extends StatefulWidget {
  final SeatBookingModel bookingData;

  ConfirmSeatScreen({this.bookingData});

  @override
  State<StatefulWidget> createState() {
    return ConfirmSeatScreenState();
  }
}

class ConfirmSeatScreenState extends StateMVC<ConfirmSeatScreen> {
  ConfirmSeatBookingScreenController _con;

  ConfirmSeatScreenState() : super(ConfirmSeatBookingScreenController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          Localization.stLocalized(
                  "confirmBooking", Localization.isEnglishCode())
              .toUpperCase(),
          style: Theme.of(context)
              .textTheme
              .headline6
              .merge(TextStyle(letterSpacing: 1.3)),
        ),
      ),
//      "assets/images/ticket_background.png",
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(children: <Widget>[
            Card(
              margin: EdgeInsets.only(left: 20, right: 20),
              elevation: 20,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              color: Colors.white,
              child: Container(
                padding: EdgeInsets.only(left: 10, right: 10, bottom: 80),
                child: Column(
                  children: <Widget>[
                    Container(
                      width: 250,
                      height: 180,
                      child: Image.asset(
                        "assets/images/app_logo.png",
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                          child: Text(
                              Localization.stLocalized("departureDateTime",
                                  Localization.isEnglishCode()),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .merge(TextStyle(
                                  color: Theme.of(context).primaryColor))),
                        ),
                        Text(
                          widget.bookingData.departureDate,
                          style: Theme.of(context).textTheme.bodyText1.merge(
                              TextStyle(
                                  color: Theme.of(context).primaryColor)),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        widget.bookingData.oneWay == "true"
                            ? Container()
                            : Expanded(
                              child: Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Text(
                                Localization.stLocalized(
                                    "arrivalDateTime",
                                    Localization.isEnglishCode()) +
                                    ":",
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6
                                    .merge(TextStyle(
                                    color: Theme.of(context)
                                        .primaryColor))),
                        ),
                            ),

                        widget.bookingData.oneWay == "true"
                            ? Container()
                            : Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Text(widget.bookingData.arrivalDate,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .merge(TextStyle(
                                  color: Theme.of(context)
                                      .primaryColor))),
                        ),

                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                                Localization.stLocalized("departureFrom",
                                    Localization.isEnglishCode()) +
                                    ":",
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6
                                    .merge(TextStyle(
                                    color:
                                    Theme.of(context).primaryColor))),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Text(widget.bookingData.departureFrom,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .merge(TextStyle(
                                  color:
                                  Theme.of(context).primaryColor))),
                        ),

                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(top: 10),
                            child: Text(
                                Localization.stLocalized("arrivalAt",
                                    Localization.isEnglishCode()) +
                                    ":",
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6
                                    .merge(TextStyle(
                                    color:
                                    Theme.of(context).primaryColor))),
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Text(widget.bookingData.arrivalAt,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .merge(TextStyle(
                                  color:
                                  Theme.of(context).primaryColor))),
                        ),

                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 14),
                          child: Text(
                              Localization.stLocalized("totalAmount",
                                  Localization.isEnglishCode()) +
                                  ":",
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .merge(TextStyle(
                                  color:
                                  Theme.of(context).primaryColor))),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 14.0),
                          child: Text(widget.bookingData.totalAmount,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .merge(TextStyle(
                                  color:
                                  Theme.of(context).primaryColor))),
                        ),


                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 9),
                          child: Text(
                              Localization.stLocalized("advanceAmount",
                                  Localization.isEnglishCode()) +
                                  ":",
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .merge(TextStyle(
                                  color:
                                  Theme.of(context).primaryColor))),
                        ),

                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Text(widget.bookingData.advanceAmount,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .merge(TextStyle(
                                  color:
                                  Theme.of(context).primaryColor))),
                        ),

                      ],
                    )
                  ],
                ),
              ),
            ),
            _circularButton(
              titleKey: "proceedToPayment",
              buttonColor: Colors.pinkAccent,
              textColor: Colors.white,
              onButtonPressed: () {
                setState(() {
                  _con.confirmTicket(widget.bookingData);

                });
              },
            ),
          ]),
        ),
      ),

    );
  }

  Widget _circularButton(
      {String titleKey,
      Function onButtonPressed,
      double width = 260,
      Color buttonColor,
      Color textColor}) {
    return Container(
      width: width,
      padding: EdgeInsets.all(6.0),
      margin: EdgeInsets.only(top: 20),
      child: RaisedButton(
        color: buttonColor,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        onPressed: () {
          onButtonPressed();
        },
        child: Padding(
          padding: const EdgeInsets.all(14.0),
          child: Text(
            Localization.stLocalized(titleKey, Localization.isEnglishCode()),
            style: TextStyle(color: textColor),
          ),
        ),
      ),
    );
  }
}
