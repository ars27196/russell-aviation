import 'package:intl/intl.dart';

class SeatBookingData {
  bool isAddress2 = false;
  String airport1 = "Key West International Airport";
  String airport2 = "Punta Gorda Airport";
  int noOfPassenger;
  int seatPriceOneWay;
  int advanceAmountOneWay;
  int amountDueOneWay;
  int seatPriceTwoWay;
  int advanceAmountTwoWay;
  int amountDueTwoWay;
  bool isOneWay;
  int seatPriceOneWayTotal;
  int advanceAmountOneWayTotal;
  int amountDueOneWayTotal;
  int seatPriceTwoWayTotal;
  int advanceAmountTwoWayTotal;
  int amountDueTwoWayTotal;

  DateTime departureDate;

  DateTime arrivalDate;

  get departureDateFormated => DateFormat("dd/mm/yyyy hh:mm a").format(departureDate);

  get arrivalDateFormated => DateFormat("dd/mm/yyyy hh:mm a").format(arrivalDate);

  SeatBookingData() {
    _initializeValues();
    calculateFare();
  }

  void _initializeValues() {
    noOfPassenger = 1;
    seatPriceOneWay = 3250;
    advanceAmountOneWay = 2000;
    amountDueOneWay = 1250;
    seatPriceTwoWay = 6500;
    advanceAmountTwoWay = 4000;
    amountDueTwoWay = 2500;
    isOneWay = true;
  }

  void calculateFare() {
    if (isOneWay) {
      seatPriceOneWayTotal = noOfPassenger * seatPriceOneWay;
      advanceAmountOneWayTotal = noOfPassenger * advanceAmountOneWay;
      amountDueOneWayTotal = noOfPassenger * amountDueOneWay;
    } else {
      seatPriceTwoWayTotal = noOfPassenger * seatPriceTwoWay;
      advanceAmountTwoWayTotal = noOfPassenger * advanceAmountTwoWay;
      amountDueTwoWayTotal = noOfPassenger * amountDueTwoWay;
    }
  }
}
