import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:russellavaition/models/user.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // sign up with User and Password
  Future registerUserWithEmailAndPassword(
      {String userEmail, String userPassword}) async {
    try {
      AuthResult registerResult = await _auth.createUserWithEmailAndPassword(
          email: userEmail, password: userPassword);
      FirebaseUser user = registerResult.user;
      return _getUserFromFirebaseObject(user, userEmail);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //sign in with email and password
  Future signInWithEmailAndPassword(
      {String userEmail, String userPassword}) async {
    try {
      AuthResult signInResult = await _auth
          .signInWithEmailAndPassword(email: userEmail, password: userPassword)
          .catchError((onError) {
        print("error :: " + onError);
      });
      FirebaseUser user = signInResult.user;
      print("user :: $user");

      return _getUserFromFirebaseObject(user, userEmail);
    } on PlatformException catch(exception){
      print("Platform exception :: " + exception.message);
    }
    catch (e) {
      print("exception :: " + e);
      return null;
    }
  }

  // reset password
  Future resetPassword({userEmail}) async {
    try {
      await _auth.sendPasswordResetEmail(email: userEmail);
    } catch (e) {
      print("Password link Error: ${e.toString()}");
    }
  }

  // get user from Firebase
  User _getUserFromFirebaseObject(FirebaseUser firebaseUser, userEmail) =>
      firebaseUser != null
          ? User(uId: firebaseUser.uid, email: userEmail)
          : null;
}
